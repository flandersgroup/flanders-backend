import {ConfigurationTest} from '@configuration/config.test';
import {ControllersInterface} from '@controller';
import {AuthorizationControllerInterface} from '@controller/authorization';
import {LoginControllerInterface} from '@controller/login';
import {LoginMailerInterface} from '@controller/login/mailer';
import {ProfileControllerInterface} from '@controller/profile';
import {LibrariesInterface} from '@lib';
import {HasherInterface} from '@lib/hasher';
import {Logger} from '@lib/logger';
import {WinstonLogger} from '@lib/logger/winston';
import {MailerInterface} from '@lib/mailer';
import {TokenInterface} from '@lib/token';
import {TokenResponse} from '@lib/token/data';
import {Authorization} from '@model/database/authorization';
import {Profile} from '@model/database/profile';
import {User} from '@model/database/user';
import {UserProfileRelation} from '@model/database/userProfileRelation';
import {RepositoriesInterface} from '@repository';
import {AuthorizationRepositoryInterface} from '@repository/authorization';
import {DatabaseConnectionInterface} from '@repository/base';
import {ProfileRepositoryInterface} from '@repository/profile';
import {UserRepositoryInterface} from '@repository/user';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import {Knex} from 'knex';

export class UnimplementedProfileRepository implements ProfileRepositoryInterface {
  getRegistrableProfiles(): Promise<Profile[]> {
    throw new Error('Method not implemented.');
  }
  isProfileRegistrable(profileId: number): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  getProfileByName(profileName : string) : Promise<Profile | undefined> {
    throw new Error('Not implemented');
  }
  getProfilesOfUser(userId : number) : Promise<Profile[]> {
    throw new Error('Method not implemented.');
  }
  insertUserProfileRelation(userProfileRelation : UserProfileRelation) : Promise<void> {
    throw new Error('Not implemented');
  }
}

export class UnimplementedUserRepository implements UserRepositoryInterface {
  insertUser(user: User): Promise<number> {
    throw new Error('Method not implemented.');
  }
  getUserByEmail(email: string): Promise<User | undefined> {
    throw new Error('Method not implemented.');
  }
  updatePassword(userId: number, password: string) : Promise<void> {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedAuthorizationRepository implements AuthorizationRepositoryInterface {
  getAuthorizations(): Promise<Authorization[]> {
    throw new Error('Method not implemented.');
  }
  getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]> {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedDatabaseConnection implements DatabaseConnectionInterface {
  get connection() : Knex {
    throw new Error('Method not implemented.');
  }
  startTransaction() : Promise<void> {
    throw new Error('Method not implemented.');
  }
  commitTransaction() : void {
    throw new Error('Method not implemented.');
  }
  rollbackTransaction() : void {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedRepositories implements RepositoriesInterface {
  get user() : UserRepositoryInterface {
    return new UnimplementedUserRepository;
  }

  get profile() : ProfileRepositoryInterface {
    return new UnimplementedProfileRepository;
  }

  get authorization() : AuthorizationRepositoryInterface {
    return new UnimplementedAuthorizationRepository;
  }

  get databaseConnection() : DatabaseConnectionInterface {
    return new UnimplementedDatabaseConnection;
  }
}

export class UnimplementedHasher implements HasherInterface {
  hashPassword(password: string): Promise<string> {
    throw new Error('Method not implemented.');
  }
  verifyPassword(password: string, hash: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedToken implements TokenInterface {
  expiresIn: number = 0;
  key: string = '';
  encode(payload: string | object): string {
    throw new Error('Method not implemented.');
  }
  encodeAndReturnExpiration(payload: string | object): TokenResponse {
    throw new Error('Method not implemented.');
  }
  decode(payload: string): string | object {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedMailer implements MailerInterface {
  from: string = '';
  to: string = '';
  subject: string = '';
  text: string = '';
  html: string = '';
  sendMail(): Promise<any> {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedLibraries implements LibrariesInterface {
  logger: Logger = WinstonLogger.createLogger(new ConfigurationTest());
  hasher: HasherInterface = new UnimplementedHasher;
  token: TokenInterface = new UnimplementedToken;
  mailer: MailerInterface = new UnimplementedMailer;
  mailerToken: TokenInterface = new UnimplementedToken;
}

export class UnimplementedLoginMailer implements LoginMailerInterface {
  sendRegistrationMail(registrationParameters: RegistrationParameters) : Promise<any> {
    throw new Error('Method not implemented.');
  }

  decodeRegistrationParameters(encodedData : string) : RegistrationParameters {
    throw new Error('Method not implemented.');
  }

  encodeRegistrationParameters(registrationParameters: RegistrationParameters) : string {
    throw new Error('Method not implemented.');
  }

  sendResetPasswordMail(resetPasswordParameters : ResetPasswordRequestParameters) : Promise<any> {
    throw new Error('Method not implemented.');
  }

  decodeResetPasswordParameters(encodedData : string) : ResetPasswordRequestParameters {
    throw new Error('Method not implemented.');
  }

  encodeResetPasswordParameters(resetPasswordParameters: ResetPasswordRequestParameters) : string {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedProfileController implements ProfileControllerInterface {
  getProfilesOfUser(userId: number): Promise<ProfileResponse[]> {
    throw new Error('Method not implemented.');
  }
  getRegistrableProfiles(): Promise<ProfileResponse[]> {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedControllers implements ControllersInterface {
  get login() : LoginControllerInterface {
    throw new Error('Method not implemented.');
  }

  get profile() : ProfileControllerInterface {
    throw new Error('Method not implemented.');
  }

  get authorization() : AuthorizationControllerInterface {
    throw new Error('Method not implemented.');
  }
}

export class UnimplementedAuthorizationController implements AuthorizationControllerInterface {
  loggedInUser(tokenData: string): Promise<User> {
    throw new Error('Method not implemented.');
  }
  getAuthorizationsOfProfile(profileName: string): Promise<AuthorizationResponse[]> {
    throw new Error('Method not implemented.');
  }
  getAuthorizationsOfUser(userId: number) : Promise<AuthorizationResponse[]> {
    throw new Error('Method not implemented.');
  }
}


