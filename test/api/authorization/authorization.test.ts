import {Application} from '@application';
import {AuthorizationControllerInterface} from '@controller/authorization';
import {AuthorizationController} from '@controller/authorization/authorization';
import {Controllers} from '@controller/controllers';
import {User} from '@model/database/user';
import {GenericRequestResponseErrorMessage} from '@server/messages';
import {AuthorizationResponse} from 'common/server/response/authorization';
import request from 'supertest';
import {UnimplementedAuthorizationController, UnimplementedControllers} from 'test/unimplemented.mock';

describe('App', function() {
  const application = new Application();


  before(async () => {
    application.setup('test');
  });

  describe('isLoggedIn api', () => {
    it('invalid token data', async function() {
      await request(application.application)
          .get('/isLoggedIn').send().expect(401).expect('Unauthorized');
    });
    it('unknown user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('email');

      await request(application.application)
          .get('/isLoggedIn').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(401).expect('Unknown user');
    });
    it('known user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      await request(application.application)
          .get('/isLoggedIn').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(200);
    });
  });

  describe('authorization api', () => {
    it('invalid token data', async function() {
      await request(application.application)
          .get('/authorization').send().expect(401).expect('Unauthorized');
    });
    it('internal error', async function() {
      const encodedData = (application.controllers?.authorization as AuthorizationController).token.encode('email');

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override loggedInUser(tokenData: string): Promise<User> {
          return new Promise<User>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      const controllers = application.controllers;

      if (application.server && application.server.authorizationInitialization) {
        application.server.authorizationInitialization.controllers = new MockControllers;
      }

      try {
        await request(application.application)
            .get('/authorization').set('Authorization', 'Bearer ' + encodedData).send().expect(500).expect('Internal error');
      } finally {
        if (application.server && application.server.authorizationInitialization && controllers) {
          application.server.authorizationInitialization.controllers = controllers;
        }
      }
    });
    it('unknown user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('email');

      await request(application.application)
          .get('/authorization').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(401).expect('Unknown user');
    });
    it('admin user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile2@test.com');

      const expectedAuthorizations : AuthorizationResponse[] = [];

      const authorization = new AuthorizationResponse;
      authorization.name = 'CanAdministrateUsers';
      authorization.description = 'Can create, update, remove user and give them access authorization';
      expectedAuthorizations.push(authorization);

      await request(application.application)
          .get('/authorization').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(200).expect(JSON.stringify(expectedAuthorizations));
    });
    it('hero user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('hero.profile3@test.com');

      const expectedAuthorizations : AuthorizationResponse[] = [];

      let authorization = new AuthorizationResponse;
      authorization.name = 'CanSeeHisSubmittedMissions';
      authorization.description = 'Can consult the missions he submitted';
      expectedAuthorizations.push(authorization);

      authorization = new AuthorizationResponse;
      authorization.name = 'CanSubmitMission';
      authorization.description = 'Can submit mission';
      expectedAuthorizations.push(authorization);

      await request(application.application)
          .get('/authorization').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(200).expect(JSON.stringify(expectedAuthorizations));
    });
  });

  describe('authorizationsOfProfile api', () => {
    it('invalid token data', async function() {
      await request(application.application)
          .get('/authorizationsOfProfile').send().expect(401).expect('Unauthorized');
    });
    it('unknown user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('email');

      await request(application.application)
          .get('/authorizationsOfProfile').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(401).expect('Unknown user');
    });
    it('missing profile name parameter', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      await request(application.application)
          .get('/authorizationsOfProfile').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(400).expect(GenericRequestResponseErrorMessage.invalidRequestParameters);
    });
    it('non authorized', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      await request(application.application)
          .get('/authorizationsOfProfile').set('Authorization', 'Bearer ' + encodedData).query({profileName: 'Hero'}).send()
          .expect(401).expect('Unauthorized');
    });
    it('success administrator', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      const authorizationResponses : AuthorizationResponse[] = [];

      const authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanAdministrateUsers';
      authorizationResponse.description = 'Can create, update, remove user and give them access authorization';

      authorizationResponses.push(authorizationResponse);

      await request(application.application)
          .get('/authorizationsOfProfile').set('Authorization', 'Bearer ' + encodedData).query({profileName: 'Administrator'}).send()
          .expect(200).expect(JSON.stringify(authorizationResponses));
    });
    it('success vilain', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      const authorizationResponses : AuthorizationResponse[] = [];

      let authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanSeeHisPlanning';
      authorizationResponse.description = 'Can consult his planning';

      authorizationResponses.push(authorizationResponse);

      authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanSeeHisAssignedMissions';
      authorizationResponse.description = 'Can consult the missions that are assigned to him';

      authorizationResponses.push(authorizationResponse);

      await request(application.application)
          .get('/authorizationsOfProfile').set('Authorization', 'Bearer ' + encodedData).query({profileName: 'Vilain'}).send()
          .expect(200).expect(JSON.stringify(authorizationResponses));
    });
  });

  after(async () => {
    await application.shutdownApplication();
  });
});
