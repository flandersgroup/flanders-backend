import request from 'supertest';
import {Application} from '@application';

describe('App', function() {
  const application = new Application();

  before(async () => {
    await application.setup('test');
  });

  it('Status api', async function() {
    let expectedBody;

    if (process.env.NODE_ENV) {
      expectedBody = {
        status: 'Ok',
        environnement: process.env.NODE_ENV,
      };
    } else {
      expectedBody = {status: 'Ok'};
    }

    await request(application.application)
        .get('/status')
        .expect(200).expect(expectedBody);
  });

  after(async () => {
    await application.shutdownApplication();
  });
});

