import request from 'supertest';
import {Application} from '@application';
import {expect} from 'chai';
import {MailerInterface} from '@lib/mailer';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {UserRepository} from '@repository/user/user';
import {User} from '@model/database/user';
import {ConfigurationTest} from '@configuration/config.test';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {Libraries} from '@lib/libraries';
import {LibrariesInterface} from '@lib';
import {Repositories} from '@repository/repositories';
import {UserRepositoryInterface} from '@repository/user';
import {LoginController} from '@controller/login/login';
import {RepositoriesInterface} from '@repository';
import {DatabaseConnection} from '@repository/base/databaseConnection';
import {ProfileRepository} from '@repository/profile/profile';
import {ProfileRepositoryInterface} from '@repository/profile';
import {UserProfileRelation} from '@model/database/userProfileRelation';
import {AuthorizationRepositoryInterface} from '@repository/authorization';
import {AuthorizationRepository} from '@repository/authorization/authorization';
import {Logger} from '@lib/logger';

describe('App', function() {
  const application = new Application();
  let libraires : LibrariesInterface | undefined = undefined;
  let repositories : RepositoriesInterface | undefined = undefined;

  before(() => {
    application.setup('test');

    libraires = application.libraries;
    repositories = application.repositories;
  });

  beforeEach(() => {
    application.libraries = libraires;
    application.repositories = repositories;
  });

  describe('Login api', () => {
    it('successful login', function(done) {
      request(application.application)
          .post('/login').send({email: 'admin.profile1@test.com', password: 'password'})
          .expect(200).end((err, res) => {
            expect(err).null;

            expect(res.body).not.undefined;
            expect(res.body.token).string;
            expect(res.body.token).not.equal('');

            expect(res.body.expires).string;
            expect(res.body.expires).not.equal('');

            done();
          });
    });
    it('failed password', async function() {
      await request(application.application)
          .post('/login').send({email: 'admin.profile1@test.com', password: 'whatever'})
          .expect(401).expect('Wrong password');
    });
    it('failed login', async function() {
      await request(application.application)
          .post('/login').send({email: 'whatever', password: 'any'})
          .expect(401).expect('Unknown user');
    });
    it('wrong parameters', async function() {
      await request(application.application)
          .post('/login').send({})
          .expect(400).expect('Invalid request parameters');
    });
  });

  describe('Register api', () => {
    it('successful registration', async function() {
      const registrationParameters : RegistrationParameters = {email: 'user1@test.com', password: 'password', profile: 'Visitor'};
      class MockMailer implements MailerInterface {
        from: string = '';
        to: string = '';
        subject: string = '';
        text: string = '';
        html: string = '';
        sendMail(): Promise<any> {
          expect(this.to).equal('user1@test.com');
          expect(this.subject).equal('Welcome to the flanders company, validate your registration');
          const expectedTextBegin = '<h1>Welcome to the flanders company</h1><div>Click on this <a href="' +
          new ConfigurationTest().loginMailerConfiguration.frontendRegistratedApi;
          const expectedTextEnd = '">link to validate your registration</a></div><div>This link will expire in 24 hours</div>';
          expect(this.html.startsWith(expectedTextBegin)).equal(true, this.html + ' do not starts with ' + expectedTextBegin);
          expect(this.html.endsWith(expectedTextEnd)).equal(true, this.html + ' do not ends with ' + expectedTextEnd);
          const tokenData = this.html.replace(expectedTextBegin, '').replace(expectedTextEnd, '');
          const decodedData : RegistrationParameters = application.libraries?.mailerToken.decode(tokenData) as RegistrationParameters;
          expect(decodedData.email).equal(registrationParameters.email);
          expect(application.libraries?.hasher.verifyPassword(registrationParameters.password, decodedData.password));
          expect(decodedData.profile).equal(registrationParameters.profile);
          return new Promise<any>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockLibraries extends Libraries {
        override get mailer() : MailerInterface {
          return new MockMailer;
        }
      }

      application.libraries = new MockLibraries(new ConfigurationTest);

      await request(application.application)
          .post('/register').send(registrationParameters)
          .expect(204);
    });
    it('alreadyRegistered', async function() {
      await request(application.application)
          .post('/register').send({email: 'admin.profile1@test.com', password: 'password', profile: 'Visitor'})
          .expect(409).expect('User already registered');
    });
    it('non registerable profile', async function() {
      await request(application.application)
          .post('/register').send({email: 'user1@test.com', password: 'password', profile: 'Administrator'})
          .expect(400).expect('The profile choosen is not registrable');
    });
    it('wrong parameters', async function() {
      await request(application.application)
          .post('/register').send({})
          .expect(400).expect('Invalid request parameters');
    });
  });

  describe('Registered api', () => {
    it('successfully registered', function(done) {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'user2@test.com';
      registrationParameters.password = 'password';
      registrationParameters.profile = 'Hero';

      const mailer = (application.controllers?.login as LoginController).loginMailerFacade.loginMailer;
      const encodedData = mailer.encodeRegistrationParameters(registrationParameters);

      const userId = 2;

      class MockUserRepository extends UserRepository {
        override insertUser(user: User): Promise<number> {
          expect(user.user_email).equal(registrationParameters.email);
          expect(user.user_password).equal(registrationParameters.password);

          return new Promise<number>((resolve) => {
            resolve(2);
          });
        }
      }

      class MockProfileRepositories extends ProfileRepository {
        insertUserProfileRelation(userProfileRelation : UserProfileRelation) : Promise<void> {
          expect(userProfileRelation.user_profile_relation_user_id).equal(userId);
          expect(userProfileRelation.user_profile_relation_profile_id).equal(4);
          return new Promise<void>((resolve) => {
            resolve();
          });
        }
      }

      class MockRepositories extends Repositories {
        get user() : UserRepositoryInterface {
          return new MockUserRepository(this.databaseConnection);
        }

        get profile() : ProfileRepositoryInterface {
          return new MockProfileRepositories(this.databaseConnection);
        }

        get authorization() : AuthorizationRepositoryInterface {
          return new AuthorizationRepository(this.databaseConnection);
        }
      }

      application.repositories = new MockRepositories(application.libraries?.logger as Logger,
          new ConfigurationTest().databaseConfiguration);

      const registeredParameter = new RegisteredParameters;
      registeredParameter.tokenData = encodedData;

      request(application.application).post('/registered').send(registeredParameter).expect(200).end(async (err, res) => {
        expect(err).null;

        expect(res.body).not.undefined;
        expect(res.body.token).string;
        expect(res.body.token).not.equal('');

        expect(res.body.expires).string;
        expect(res.body.expires).not.equal('');

        done();
      });
    });
    it('already registered', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'hero.profile3@test.com';
      registrationParameters.password = 'password';
      registrationParameters.profile = 'Visitor';

      const mailer = (application.controllers?.login as LoginController).loginMailerFacade.loginMailer;

      const encodedData = mailer.encodeRegistrationParameters(registrationParameters);

      const registeredParameter = new RegisteredParameters;
      registeredParameter.tokenData = encodedData;

      await request(application.application).post('/registered').send(registeredParameter).expect(409).expect('User already registered');
    });
    it('non registrable profile', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'user2@test.com';
      registrationParameters.password = 'password';
      registrationParameters.profile = 'Administrator';

      const mailer = (application.controllers?.login as LoginController).loginMailerFacade.loginMailer;
      const encodedData = mailer.encodeRegistrationParameters(registrationParameters);

      const registeredParameter = new RegisteredParameters;
      registeredParameter.tokenData = encodedData;

      await request(application.application).post('/registered').send(registeredParameter)
          .expect(400).expect('The profile choosen is not registrable');
    });
    it('bad data', async function() {
      const registeredParameter = new RegisteredParameters;
      registeredParameter.tokenData = 'whatever';

      await request(application.application).post('/registered').send(registeredParameter)
          .expect(400).expect('The given token data are not valid');
    });
    it('wrong parameters', async function() {
      await request(application.application).post('/registered').send({}).expect(400).expect('Invalid request parameters');
    });
  });

  describe('Reset password request api', () => {
    it('successfull reset password request', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'visitor.profile3@test.com';

      class MockMailer implements MailerInterface {
        from: string = '';
        to: string = '';
        subject: string = '';
        text: string = '';
        html: string = '';
        sendMail(): Promise<any> {
          expect(this.to).equal(resetPasswordRequestParameters.email);
          expect(this.subject).equal('Flanders company, your password reset request');
          const expectedTextBegin = '<h1>You send a request to reset your password.</h1><div>Click on this <a href="' +
          new ConfigurationTest().loginMailerConfiguration.frontendResetPasswordApi;
          const expectedTextEnd = '">link to reset your password</a></div><div>This link will expire in 24 hours</div>';
          expect(this.html.startsWith(expectedTextBegin)).equal(true, this.html + ' do not starts with ' + expectedTextBegin);
          expect(this.html.endsWith(expectedTextEnd)).equal(true, this.html + ' do not ends with ' + expectedTextEnd);
          const tokenData = this.html.replace(expectedTextBegin, '').replace(expectedTextEnd, '');
          const decodedData : ResetPasswordRequestParameters =
          application.libraries?.mailerToken.decode(tokenData) as ResetPasswordRequestParameters;
          expect(decodedData.email).equals(resetPasswordRequestParameters.email);
          return new Promise<any>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockLibraries extends Libraries {
        override get mailer() : MailerInterface {
          return new MockMailer;
        }
      }

      application.libraries = new MockLibraries(new ConfigurationTest);

      await request(application.application).post('/resetPasswordRequest').send(resetPasswordRequestParameters).expect(204);
    });
    it('unknow User', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'whatever';

      await request(application.application).post('/resetPasswordRequest').send(resetPasswordRequestParameters)
          .expect(404).expect('Cannot find the given user email');
    });
    it('wrong parameters', async function() {
      await request(application.application).post('/resetPasswordRequest').send({}).expect(400).expect('Invalid request parameters');
    });
  });

  describe('Reset password api', () => {
    it('successfull reset password request', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'visitor.profile3@test.com';

      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = application.libraries?.mailerToken.encode(resetPasswordRequestParameters) ?? '';
      resetPasswordParameters.password = 'anewPassword';

      class MockUserRepository extends UserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equals(resetPasswordRequestParameters.email);

          const user = new User;
          user.user_id = 42;
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }

        override updatePassword(userId: number, password: string) : Promise<void> {
          expect(userId).equal(42);
          expect(password).not.equals(resetPasswordParameters.password);
          expect(application.libraries?.hasher.verifyPassword(password, resetPasswordParameters.password));

          return new Promise<void>((resolve) => {
            resolve();
          });
        }
      }

      const libraries = application.libraries as LibrariesInterface;
      const configuration = new ConfigurationTest;

      class MockRepositories extends Repositories {
        override get user() : UserRepositoryInterface {
          return new MockUserRepository(new DatabaseConnection(new ConfigurationTest));
        }
      }

      application.repositories = new MockRepositories(libraries.logger, configuration.databaseConfiguration);

      await request(application.application).post('/resetPassword').send(resetPasswordParameters).expect(204);

      application.repositories = new Repositories(libraries.logger, configuration.databaseConfiguration);
    });

    it('unknown user', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'unknownMail@mail.com';

      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = application.libraries?.mailerToken.encode(resetPasswordRequestParameters) ?? '';
      resetPasswordParameters.password = 'anewPassword';

      await request(application.application).post('/resetPassword').send(resetPasswordParameters)
          .expect(404).expect('Cannot find the given user email');
    });
    it('wrong parameters', async function() {
      await request(application.application).post('/resetPassword').send({}).expect(400).expect('Invalid request parameters');
    });
  });

  after(async () => {
    await application.shutdownApplication();
  });
});
