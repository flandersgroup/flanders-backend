import request from 'supertest';
import {Application} from '@application';
import {Controllers} from '@controller/controllers';
import {AuthorizationController} from '@controller/authorization/authorization';
import {ProfileResponse} from 'common/server/response/profile';

describe('App', function() {
  const application = new Application();

  before(async () => {
    await application.setup('test');
  });

  describe('Profile api', function() {
    it('registrableProfiles', async function() {
      const expectedBody = [
        {
          'name': 'Visitor',
          'description': 'A visitor',
        },
        {
          'name': 'Hero',
          'description': 'A hero who can submit mission for vilains',
        },
        {
          'name': 'Candidate',
          'description': 'A candidate to become a vilain',
        },
      ];

      await request(application.application)
          .get('/registrableProfiles')
          .expect(200).expect(expectedBody);
    });
  });

  describe('profilesOfUser api', () => {
    it('invalid token data', async function() {
      await request(application.application)
          .get('/profilesOfUser').send().expect(401).expect('Unauthorized');
    });
    it('unknown user', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('email');

      await request(application.application)
          .get('/profilesOfUser').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(401).expect('Unknown user');
    });
    it('success', async function() {
      const controllers = application.controllers as Controllers;
      const authorizationController = controllers.authorization as AuthorizationController;

      const encodedData = authorizationController.token.encode('admin.profile1@test.com');

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      await request(application.application)
          .get('/profilesOfUser').set('Authorization', 'Bearer ' + encodedData).send()
          .expect(200).expect(JSON.stringify(profiles));
    });
  });

  after(async () => {
    await application.shutdownApplication();
  });
});

