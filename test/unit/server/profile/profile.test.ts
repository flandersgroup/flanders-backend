import {ConfigurationTest} from '@configuration/config.test';
import {ProfileControllerInterface} from '@controller/profile';
import {Libraries} from '@lib/libraries';
import {User} from '@model/database/user';
import {Profile} from '@server/profile/profile';
import {expect} from 'chai';
import {ProfileResponse} from 'common/server/response/profile';
import express from 'express';
import request from 'supertest';
import {UnimplementedControllers, UnimplementedProfileController} from 'test/unimplemented.mock';

describe('server.profile', function() {
  const libraries = new Libraries(new ConfigurationTest);
  const application = express();
  application.use(express.json());

  const profileServer = new Profile(libraries.logger, new UnimplementedControllers);

  describe('getRegistrableProfiles', function() {
    before(() => {
      application.get('/getRegistrableProfiles',
          (request, response) => {
            profileServer.registrableProfiles(request, response);
          });
    });

    it('success', async function() {
      const profileResponses : ProfileResponse[] = [];

      const profileResponse = new ProfileResponse;

      profileResponse.description = 'The answer';
      profileResponses.push(profileResponse);

      profileResponse.description = 'Twenty one';
      profileResponses.push(profileResponse);

      class MockProfileController extends UnimplementedProfileController {
        override getRegistrableProfiles(): Promise<ProfileResponse[]> {
          return new Promise<ProfileResponse[]>((resolve) => resolve(profileResponses));
        }
      }

      class MockControllers extends UnimplementedControllers {
        get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      profileServer.controllers = new MockControllers;

      await request(application)
          .get('/getRegistrableProfiles')
          .expect(200).expect(JSON.stringify(profileResponses));
    });
    it('internal error', async function() {
      class MockProfileController extends UnimplementedProfileController {
        override getRegistrableProfiles(): Promise<ProfileResponse[]> {
          return new Promise<ProfileResponse[]>((resolve, reject) => reject(new Error('Internal error')));
        }
      }

      class MockControllers extends UnimplementedControllers {
        get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      profileServer.controllers = new MockControllers;

      await request(application)
          .get('/getRegistrableProfiles')
          .expect(500).expect('Internal error');
    });
  });
  describe('profilesOfUser', function() {
    let user: User | undefined = undefined;
    before(() => {
      application.get('/profilesOfUser',
          (request, response, next) => {
            response.locals.user = user;
            next();
          },
          (request, response) => {
            profileServer.profilesOfUser(request, response);
          });
    });
    it('internal error null user', async function() {
      user = undefined;
      await request(application)
          .get('/profilesOfUser').send().expect(500).expect('Unexpected error');
    });
    it('getProfilesOfUser error', async function() {
      user = new User;
      user.user_email = 'whatever';
      user.user_id = 21;

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      profileServer.controllers = new MockControllers;

      await request(application)
          .get('/profilesOfUser').send().expect(500).expect('Internal error');
    });
    it('success', async function() {
      user = new User;
      user.user_email = 'whatever';
      user.user_id = 21;

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      profileServer.controllers = new MockControllers;

      await request(application)
          .get('/profilesOfUser').send().expect(200).expect(JSON.stringify(profiles));
    });
  });
});
