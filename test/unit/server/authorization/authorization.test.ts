import {ConfigurationTest} from '@configuration/config.test';
import {AuthorizationControllerInterface} from '@controller/authorization';
import {AuthorizationError} from '@controller/authorization/error';
import {ProfileControllerInterface} from '@controller/profile';
import {Libraries} from '@lib/libraries';
import {User} from '@model/database/user';
import {Authorization} from '@server/autorization/autorization';
import {GenericRequestResponseErrorMessage} from '@server/messages';
import {expect} from 'chai';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import express from 'express';
import request from 'supertest';
import {UnimplementedAuthorizationController, UnimplementedControllers, UnimplementedProfileController} from 'test/unimplemented.mock';

describe('server.authorization', function() {
  const libraries = new Libraries(new ConfigurationTest);
  const application = express();

  const authorization = new Authorization(libraries.logger, new UnimplementedControllers);

  describe('userAuthorization', function() {
    let user: User | undefined;
    before(() => {
      application.get('/userAuthorization',
          (request, response, next) => {
            authorization.userAuthorization(request, response, next);
          },
          (request, response) => {
            user = response.locals.user;
            response.status(200).send();
          });
    });

    it('invalid token data', async function() {
      authorization.controllers = new UnimplementedControllers;

      await request(application)
          .get('/userAuthorization').send().expect(401).expect('Unauthorized');
    });
    it('internal error', async function() {
      const token = 'aToken';

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override loggedInUser(tokenData: string): Promise<User> {
          expect(tokenData).equal(token);
          return new Promise<User>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/userAuthorization').set('Authorization', 'Bearer ' + token).send().expect(500).expect('Internal error');
    });
    it('unknown user', async function() {
      const token = 'aToken';

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        loggedInUser(tokenData: string): Promise<User> {
          expect(tokenData).equal(token);
          return new Promise<User>((resolve, reject) => {
            reject(new AuthorizationError('Unknown user'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/userAuthorization').set('Authorization', 'Bearer ' + token).send()
          .expect(401).expect('Unknown user');
    });
    it('known user', async function() {
      const token = 'aToken';

      const expectedUser = new User;
      expectedUser.user_email = 'aMail';
      expectedUser.user_enabled = true;
      expectedUser.user_first_name = 'FirstName';
      expectedUser.user_last_name = 'LastName';
      expectedUser.user_id = 4;
      // expectedUser.user_profile = 2;

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        loggedInUser(tokenData: string): Promise<User> {
          expect(tokenData).equal(token);
          return new Promise<User>((resolve) => {
            resolve(expectedUser);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/userAuthorization').set('Authorization', 'Bearer ' + token).send()
          .expect(200);
      expect(user).to.eql(expectedUser);
    });
  });

  describe('isLoggedIn', async function() {
    application.get('/isLoggedIn',
        (request, response, next) => {
          authorization.userAuthorization(request, response, next);
        },
        (request, response) => {
          response.status(200).send();
        });

    await request(application)
        .get('/isLoggedIn').send().expect(200);
  });

  describe('getAuthorization', function() {
    let user: User | undefined = undefined;
    before(() => {
      application.get('/getAuthorization',
          (request, response, next) => {
            response.locals.user = user;
            next();
          },
          (request, response) => {
            authorization.getAuthorization(request, response);
          });
    });

    it('internal error null user', async function() {
      user = undefined;
      await request(application)
          .get('/getAuthorization').send().expect(500).expect('Unexpected error');
    });
    it('internal error', async function() {
      user = new User;
      user.user_email = 'whatever';
      user.user_id = 21;

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override getAuthorizationsOfUser(userId: number) : Promise<AuthorizationResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<AuthorizationResponse[]>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorization').send().expect(500).expect('Internal error');
    });
    it('success', async function() {
      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      const expectedAuthorizations : AuthorizationResponse[] = [];

      const authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanAdministrateUsers';
      authorizationResponse.description = 'Can create, update, remove user and give them access authorization';
      expectedAuthorizations.push(authorizationResponse);

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override getAuthorizationsOfUser(userId: number) : Promise<AuthorizationResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<AuthorizationResponse[]>((resolve) => {
            resolve(expectedAuthorizations);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get authorization() : AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorization').send()
          .expect(200).expect(JSON.stringify(expectedAuthorizations));
    });
  });
  describe('getAuthorizationsOfProfile', function() {
    let user: User | undefined = undefined;
    before(() => {
      application.get('/getAuthorizationsOfProfile',
          (request, response, next) => {
            response.locals.user = user;
            next();
          },
          (request, response) => {
            authorization.getAuthorizationsOfProfile(request, response);
          });
    });

    it('internal error null user', async function() {
      user = undefined;
      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: 'Administrator'}).send().expect(500).expect('Unexpected error');
    });
    it('missing profile name', async function() {
      await request(application)
          .get('/getAuthorizationsOfProfile').send().expect(400).expect(GenericRequestResponseErrorMessage.invalidRequestParameters);
    });
    it('internal error on getProfilesOfUser', async function() {
      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: 'Administrator'}).send().expect(500).expect('Internal error');
    });
    it('Unauthorized profile', async function() {
      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve, reject) => {
            resolve(profiles);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: 'Hero'}).send().expect(401).expect('Unauthorized');
    });
    it('internal error on getAuthorizationsOfProfile', async function() {
      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve, reject) => {
            resolve(profiles);
          });
        }
      }

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override getAuthorizationsOfProfile(profileName: string): Promise<AuthorizationResponse[]> {
          return new Promise<AuthorizationResponse[]>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
        override get authorization(): AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: 'Administrator'}).send().expect(500).expect('Internal error');
    });
    it('AuthorizationError on getAuthorizationsOfProfile', async function() {
      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      const sendProfile = 'Administrator';

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve, reject) => {
            resolve(profiles);
          });
        }
      }

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override getAuthorizationsOfProfile(profileName: string): Promise<AuthorizationResponse[]> {
          expect(profileName).equal(sendProfile);
          return new Promise<AuthorizationResponse[]>((resolve, reject) => {
            reject(new AuthorizationError('Unknown profile'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
        override get authorization(): AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: sendProfile}).send().expect(401).expect('Unknown profile');
    });
    it('Success', async function() {
      const sendProfile : string = 'Administrator';

      user = new User;
      user.user_email = 'admin.profile1@test.com';
      user.user_id = 21;

      const profiles : ProfileResponse[] = [];

      let profile : ProfileResponse = new ProfileResponse;
      profile.name = 'Administrator';
      profile.description = 'Administrator of the website, able to administrate users';

      profiles.push(profile);

      profile = new ProfileResponse;
      profile.name = 'Vilain';
      profile.description = 'A vilain who can edit his profile and receive missions';

      profiles.push(profile);

      const authorizationResponses : AuthorizationResponse[] = [];

      let authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanDoWhatever';
      authorizationResponse.description = 'Can do whatever';

      authorizationResponses.push(authorizationResponse);

      authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = 'CanDoSomething';
      authorizationResponse.description = 'Can do something';

      authorizationResponses.push(authorizationResponse);

      class MockProfileController extends UnimplementedProfileController {
        override getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
          expect(userId).equal(user?.user_id);
          return new Promise<ProfileResponse[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      class MockAuthorizationController extends UnimplementedAuthorizationController {
        override getAuthorizationsOfProfile(profileName: string): Promise<AuthorizationResponse[]> {
          expect(profileName).equal(sendProfile);
          return new Promise<AuthorizationResponse[]>((resolve) => {
            resolve(authorizationResponses);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        override get profile() : ProfileControllerInterface {
          return new MockProfileController;
        }
        override get authorization(): AuthorizationControllerInterface {
          return new MockAuthorizationController;
        }
      }

      authorization.controllers = new MockControllers;

      await request(application)
          .get('/getAuthorizationsOfProfile').query({profileName: sendProfile}).send()
          .expect(200).expect(JSON.stringify(authorizationResponses));
    });
  });
});
