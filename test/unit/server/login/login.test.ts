import {ConfigurationTest} from '@configuration/config.test';
import {ControllersInterface} from '@controller';
import {AuthorizationControllerInterface} from '@controller/authorization';
import {LoginControllerInterface} from '@controller/login';
import {AuthenticationError, RegisteredError, RegisteredParametersError,
  RegistrationError, RegistrationParametersError, ResetPasswordError,
  ResetPasswordParametersError, ResetPasswordRequestError, ResetPasswordRequestParametersError} from '@controller/login/error';
import {ProfileControllerInterface} from '@controller/profile';
import {Libraries} from '@lib/libraries';
import {Login} from '@server/login/login';
import {expect} from 'chai';
import {AuthenticationParameters} from 'common/server/parameter/authentication';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {LoginResponse} from 'common/server/response/login';
import express from 'express';
import request from 'supertest';

describe('server.login', function() {
  const libraries = new Libraries(new ConfigurationTest);
  const application = express();
  application.use(express.json());

  class UnimplementedLoginController implements LoginControllerInterface {
    login(email: string, password: string): Promise<LoginResponse> {
      throw new Error('Method not implemented.');
    }
    register(registrationParameters: RegistrationParameters): Promise<void> {
      throw new Error('Method not implemented.');
    }
    registered(tokenData: string): Promise<LoginResponse> {
      throw new Error('Method not implemented.');
    }
    resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
      throw new Error('Method not implemented.');
    }
    resetPassword(tokenData: string, password: string): Promise<void> {
      throw new Error('Method not implemented.');
    }
  }

  class UnimplementedControllers implements ControllersInterface {
    get login() : LoginControllerInterface {
      throw new Error('Method not implemented.');
    }

    get profile() : ProfileControllerInterface {
      throw new Error('Method not implemented.');
    }

    get authorization() : AuthorizationControllerInterface {
      throw new Error('Method not implemented.');
    }
  }

  const login = new Login(libraries.logger, new UnimplementedControllers);

  describe('login', function() {
    before(() => {
      application.post('/login',
          (request, response) => {
            login.login(request, response);
          });
    });

    it('successful login', async function() {
      const authenticationParameters = new AuthenticationParameters;
      authenticationParameters.email = 'admin.profile1@test.com';
      authenticationParameters.password = 'password';

      const loginResponse = new LoginResponse;
      loginResponse.token = 'aToken';
      loginResponse.expires = new Date(Date.now());

      class MockLoginController extends UnimplementedLoginController {
        login(email: string, password: string): Promise<LoginResponse> {
          expect(email).equal(authenticationParameters.email);
          expect(password).equal(authenticationParameters.password);
          return new Promise<LoginResponse>((resolve) => {
            resolve(loginResponse);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/login').send(authenticationParameters).expect(200).expect(JSON.stringify(loginResponse));
    });
    it('authentication error', async function() {
      const authenticationParameters = new AuthenticationParameters;
      authenticationParameters.email = 'admin.profile1@test.com';
      authenticationParameters.password = 'password';

      const loginResponse = new LoginResponse;
      loginResponse.token = 'aToken';
      loginResponse.expires = new Date(Date.now());

      class MockLoginController extends UnimplementedLoginController {
        login(email: string, password: string): Promise<LoginResponse> {
          expect(email).equal(authenticationParameters.email);
          expect(password).equal(authenticationParameters.password);
          return new Promise<LoginResponse>((resolve, reject) => {
            reject(new AuthenticationError('Wrong password'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/login').send(authenticationParameters).expect(401).expect('Wrong password');
    });
    it('wrong parameters', async function() {
      await request(application)
          .post('/login').send({})
          .expect(400).expect('Invalid request parameters');
    });
    it('internal error', async function() {
      class MockLoginController extends UnimplementedLoginController {
        override login(email: string, password: string): Promise<LoginResponse> {
          return new Promise<LoginResponse>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application)
          .post('/login').send({email: 'admin.profile1@test.com', password: 'whatever'}).expect(500).expect('Internal error');
    });
  });
  describe('register', function() {
    before(() => {
      application.post('/register',
          (request, response) => {
            login.register(request, response);
          });
    });

    it('successful registration', async function() {
      const sendRegistrationParameters = new RegistrationParameters;
      sendRegistrationParameters.email = 'aMail';
      sendRegistrationParameters.password = 'password';
      sendRegistrationParameters.profile = 'Vilain';

      class MockLoginController extends UnimplementedLoginController {
        override register(registrationParameters: RegistrationParameters): Promise<void> {
          expect(registrationParameters).to.eql(sendRegistrationParameters);
          return new Promise<void>((resolve)=>{
            resolve();
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/register').send(sendRegistrationParameters).expect(204);
    });

    it('registration error', async function() {
      const sendRegistrationParameters = new RegistrationParameters;
      sendRegistrationParameters.email = 'aMail';
      sendRegistrationParameters.password = 'password';
      sendRegistrationParameters.profile = 'Vilain';

      class MockLoginController extends UnimplementedLoginController {
        override register(registrationParameters: RegistrationParameters): Promise<void> {
          expect(registrationParameters).to.eql(sendRegistrationParameters);
          return new Promise<void>((resolve, reject)=>{
            reject(new RegistrationError('User already registered'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/register').send(sendRegistrationParameters).expect(409).expect('User already registered');
    });

    it('registration parameters error', async function() {
      const sendRegistrationParameters = new RegistrationParameters;
      sendRegistrationParameters.email = 'aMail';
      sendRegistrationParameters.password = 'password';
      sendRegistrationParameters.profile = 'Vilain';

      class MockLoginController extends UnimplementedLoginController {
        override register(registrationParameters: RegistrationParameters): Promise<void> {
          expect(registrationParameters).to.eql(sendRegistrationParameters);
          return new Promise<void>((resolve, reject)=>{
            reject(new RegistrationParametersError('The profile choosen is not registrable'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/register').send(sendRegistrationParameters)
          .expect(400).expect('The profile choosen is not registrable');
    });

    it('wrong parameters', async function() {
      await request(application).post('/register').send().expect(400).expect('Invalid request parameters');
    });

    it('internal error', async function() {
      const sendRegistrationParameters = new RegistrationParameters;
      sendRegistrationParameters.email = 'aMail';
      sendRegistrationParameters.password = 'password';
      sendRegistrationParameters.profile = 'Vilain';

      class MockLoginController extends UnimplementedLoginController {
        override register(registrationParameters: RegistrationParameters): Promise<void> {
          expect(registrationParameters).to.eql(sendRegistrationParameters);
          return new Promise<void>((resolve, reject) => {
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/register').send(sendRegistrationParameters).expect(500).expect('Internal error');
    });
  });

  describe('registered', function() {
    before(() => {
      application.post('/registered',
          (request, response) => {
            login.registered(request, response);
          });
    });

    it('successfully registered', async function() {
      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'tokenData';

      const loginResponse = new LoginResponse;
      loginResponse.token = 'aToken';
      loginResponse.expires = new Date(Date.now());

      class MockLoginController extends UnimplementedLoginController {
        override registered(tokenData: string): Promise<LoginResponse> {
          expect(tokenData).to.eql(registeredParameters.tokenData);
          return new Promise<LoginResponse>((resolve)=>{
            resolve(loginResponse);
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/registered').send(registeredParameters).expect(200).expect(JSON.stringify(loginResponse));
    });

    it('registered error', async function() {
      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'tokenData';

      class MockLoginController extends UnimplementedLoginController {
        override registered(tokenData: string): Promise<LoginResponse> {
          expect(tokenData).to.eql(registeredParameters.tokenData);
          return new Promise<LoginResponse>((resolve, reject)=>{
            reject(new RegisteredError('User already registered'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/registered').send(registeredParameters).expect(409).expect('User already registered');
    });

    it('registered parameters error', async function() {
      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'tokenData';

      class MockLoginController extends UnimplementedLoginController {
        override registered(tokenData: string): Promise<LoginResponse> {
          expect(tokenData).to.eql(registeredParameters.tokenData);
          return new Promise<LoginResponse>((resolve, reject)=>{
            reject(new RegisteredParametersError('The given token data are not valid'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/registered').send(registeredParameters).expect(400).expect('The given token data are not valid');
    });

    it('wrong parameters', async function() {
      await request(application).post('/registered').send().expect(400).expect('Invalid request parameters');
    });

    it('internal error', async function() {
      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'tokenData';

      class MockLoginController extends UnimplementedLoginController {
        override registered(tokenData: string): Promise<LoginResponse> {
          expect(tokenData).to.eql(registeredParameters.tokenData);
          return new Promise<LoginResponse>((resolve, reject)=>{
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/registered').send(registeredParameters).expect(500).expect('Internal error');
    });
  });

  describe('resetPasswordRequest', function() {
    before(() => {
      application.post('/resetPasswordRequest',
          (request, response) => {
            login.resetPasswordRequest(request, response);
          });
    });

    it('successfull reset password request', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'email';

      class MockLoginController extends UnimplementedLoginController {
        override resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
          expect(resetPasswordParameters).to.eql(resetPasswordRequestParameters);
          return new Promise<void>((resolve)=>{
            resolve();
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPasswordRequest').send(resetPasswordRequestParameters).expect(204);
    });

    it('reset password request error', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'email';

      class MockLoginController extends UnimplementedLoginController {
        override resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
          expect(resetPasswordParameters).to.eql(resetPasswordRequestParameters);
          return new Promise<void>((resolve, reject)=>{
            reject(new ResetPasswordRequestError('User not found'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPasswordRequest').send(resetPasswordRequestParameters).expect(404).expect('User not found');
    });

    it('reset password parameter request error', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'email';

      class MockLoginController extends UnimplementedLoginController {
        override resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
          expect(resetPasswordParameters).to.eql(resetPasswordRequestParameters);
          return new Promise<void>((resolve, reject)=>{
            reject(new ResetPasswordRequestParametersError('Email is required'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPasswordRequest').send(resetPasswordRequestParameters).expect(400).expect('Email is required');
    });

    it('wrong parameters', async function() {
      await request(application).post('/resetPasswordRequest').send().expect(400).expect('Invalid request parameters');
    });

    it('internal error', async function() {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = 'email';

      class MockLoginController extends UnimplementedLoginController {
        override resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
          expect(resetPasswordParameters).to.eql(resetPasswordRequestParameters);
          return new Promise<void>((resolve, reject)=>{
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPasswordRequest').send(resetPasswordRequestParameters).expect(500).expect('Internal error');
    });
  });

  describe('resetPassword', function() {
    before(() => {
      application.post('/resetPassword',
          (request, response) => {
            login.resetPassword(request, response);
          });
    });

    it('successfully reset password', async function() {
      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = 'aToken';
      resetPasswordParameters.password = 'aPassword';

      class MockLoginController extends UnimplementedLoginController {
        override resetPassword(tokenData: string, password: string): Promise<void> {
          expect(tokenData).equal(resetPasswordParameters.tokenData);
          expect(password).equal(resetPasswordParameters.password);
          return new Promise<void>((resolve)=>{
            resolve();
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPassword').send(resetPasswordParameters).expect(204);
    });

    it('reset password error', async function() {
      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = 'aToken';
      resetPasswordParameters.password = 'aPassword';

      class MockLoginController extends UnimplementedLoginController {
        override resetPassword(tokenData: string, password: string): Promise<void> {
          expect(tokenData).equal(resetPasswordParameters.tokenData);
          expect(password).equal(resetPasswordParameters.password);
          return new Promise<void>((resolve, reject)=>{
            reject(new ResetPasswordError('User not found'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPassword').send(resetPasswordParameters).expect(404).expect('User not found');
    });

    it('reset password parameter error', async function() {
      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = 'aToken';
      resetPasswordParameters.password = 'aPassword';

      class MockLoginController extends UnimplementedLoginController {
        override resetPassword(tokenData: string, password: string): Promise<void> {
          expect(tokenData).equal(resetPasswordParameters.tokenData);
          expect(password).equal(resetPasswordParameters.password);
          return new Promise<void>((resolve, reject)=>{
            reject(new ResetPasswordParametersError('Token is invalid'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPassword').send(resetPasswordParameters).expect(400).expect('Token is invalid');
    });

    it('wrong parameters', async function() {
      await request(application).post('/resetPassword').send().expect(400).expect('Invalid request parameters');
    });

    it('internal error', async function() {
      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = 'aToken';
      resetPasswordParameters.password = 'aPassword';

      class MockLoginController extends UnimplementedLoginController {
        override resetPassword(tokenData: string, password: string): Promise<void> {
          expect(tokenData).equal(resetPasswordParameters.tokenData);
          expect(password).equal(resetPasswordParameters.password);
          return new Promise<void>((resolve, reject)=>{
            reject(new Error('Internal error'));
          });
        }
      }

      class MockControllers extends UnimplementedControllers {
        get login() : LoginControllerInterface {
          return new MockLoginController;
        }
      }

      login.controllers = new MockControllers;

      await request(application).post('/resetPassword').send(resetPasswordParameters).expect(500).expect('Internal error');
    });
  });
});
