import {ConfigurationTest} from '@configuration/config.test';
import {LoginMailer} from '@controller/login/mailer/mailer';
import {MailerInterface} from '@lib/mailer';
import {TokenInterface} from '@lib/token';
import {TokenResponse} from '@lib/token/data';
import {expect} from 'chai';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {UnimplementedMailer} from '../../../../unimplemented.mock';

describe('controller.login.mailer', function() {
  it('sendRegistrationMail', async function() {
    const registrationParameters = new RegistrationParameters;
    registrationParameters.email = 'user1@test.com';
    registrationParameters.password = 'password';
    registrationParameters.profile = 'Visitor';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        expect(payload as RegistrationParameters).equal(registrationParameters);
        return encodedString;
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        throw new Error('Method not implemented.');
      }
    }

    class MockMailer implements MailerInterface {
      from: string = '';
      to: string = '';
      subject: string = '';
      text: string = '';
      html: string = '';
      sendMail(): Promise<any> {
        expect(this.to).equal('user1@test.com');
        expect(this.subject).equal('Welcome to the flanders company, validate your registration');

        let expectedText = '<h1>Welcome to the flanders company</h1><div>Click on this <a href="';
        expectedText += new ConfigurationTest().loginMailerConfiguration.frontendRegistratedApi + encodedString;
        expectedText += '">link to validate your registration</a></div><div>This link will expire in 24 hours</div>';

        expect(this.html).equal(expectedText);

        return new Promise<any>((resolve) => {
          resolve(undefined);
        });
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new MockMailer, new MockToken);

    mailer.sendRegistrationMail(registrationParameters);
  });
  it('decodeRegistrationParameters', function() {
    const registrationParameters = new RegistrationParameters;
    registrationParameters.email = 'user1@test.com';
    registrationParameters.password = 'password';
    registrationParameters.profile = 'Visitor';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        throw new Error('Method not implemented.');
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        expect(payload).equal(encodedString);
        return registrationParameters;
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new UnimplementedMailer, new MockToken);

    mailer.decodeResetPasswordParameters(encodedString);
  });
  it('decodeRegistrationParameters', function() {
    const registrationParameters = new RegistrationParameters;
    registrationParameters.email = 'user1@test.com';
    registrationParameters.password = 'password';
    registrationParameters.profile = 'Visitor';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        throw new Error('Method not implemented.');
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        expect(payload).equal(encodedString);
        return registrationParameters;
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new UnimplementedMailer, new MockToken);

    expect(mailer.decodeResetPasswordParameters(encodedString) as RegistrationParameters).equal(registrationParameters);
  });
  it('encodeRegistrationParameters', function() {
    const registrationParameters = new RegistrationParameters;
    registrationParameters.email = 'user1@test.com';
    registrationParameters.password = 'password';
    registrationParameters.profile = 'Visitor';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        expect(payload as RegistrationParameters).equal(registrationParameters);

        return encodedString;
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        throw new Error('Method not implemented.');
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new UnimplementedMailer, new MockToken);

    expect(mailer.encodeRegistrationParameters(registrationParameters)).equal(encodedString);
  });
  it('sendResetPasswordMail', async function() {
    const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
    resetPasswordRequestParameters.email = 'user1@test.com';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        expect(payload as RegistrationParameters).equal(resetPasswordRequestParameters);
        return encodedString;
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        throw new Error('Method not implemented.');
      }
    }

    class MockMailer implements MailerInterface {
      from: string = '';
      to: string = '';
      subject: string = '';
      text: string = '';
      html: string = '';
      sendMail(): Promise<any> {
        expect(this.to).equal('user1@test.com');
        expect(this.subject).equal('Flanders company, your password reset request');

        const frontendApi = new ConfigurationTest().loginMailerConfiguration.frontendResetPasswordApi;

        const expectedText = '<h1>You send a request to reset your password.</h1>'+
        `<div>Click on this <a href="${frontendApi + encodedString}">link to reset your password</a>`+
        '</div><div>This link will expire in 24 hours</div>';

        expect(this.html).equal(expectedText);

        return new Promise<any>((resolve) => {
          resolve(undefined);
        });
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new MockMailer, new MockToken);

    mailer.sendResetPasswordMail(resetPasswordRequestParameters);
  });
  it('decodeResetPasswordParameters', function() {
    const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
    resetPasswordRequestParameters.email = 'user1@test.com';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        throw new Error('Method not implemented.');
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        expect(payload).equal(encodedString);
        return resetPasswordRequestParameters;
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new UnimplementedMailer, new MockToken);

    expect(mailer.decodeResetPasswordParameters(encodedString) as ResetPasswordRequestParameters).equal(resetPasswordRequestParameters);
  });
  it('encodeResetPasswordParameters', function() {
    const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
    resetPasswordRequestParameters.email = 'user1@test.com';

    const encodedString = 'Encoded';

    class MockToken implements TokenInterface {
      expiresIn : number = 42;
      key : string = '';

      encode(payload: object | string) : string {
        expect(payload as ResetPasswordRequestParameters).equal(resetPasswordRequestParameters);

        return encodedString;
      }

      encodeAndReturnExpiration(payload: object | string) : TokenResponse {
        throw new Error('Method not implemented.');
      }

      decode(payload: string) : object | string {
        throw new Error('Method not implemented.');
      }
    }

    const mailer = new LoginMailer(new ConfigurationTest().loginMailerConfiguration, new UnimplementedMailer, new MockToken);

    expect(mailer.encodeResetPasswordParameters(resetPasswordRequestParameters)).equal(encodedString);
  });
});
