import {ConfigurationTest} from '@configuration/config.test';
import {
  AuthenticationError, RegisteredError, RegisteredParametersError,
  RegistrationError, RegistrationParametersError, ResetPasswordError,
  ResetPasswordParametersError, ResetPasswordRequestError, ResetPasswordRequestParametersError,
} from '@controller/login/error';
import {LoginController} from '@controller/login/login';
import {LoginMailerInterface} from '@controller/login/mailer';
import {LoginMailerFacade} from '@controller/login/mailer/loginMailerFacade';
import {HasherInterface} from '@lib/hasher';
import {TokenResponse} from '@lib/token/data';
import {User} from '@model/database/user';
import {ProfileRepositoryInterface} from '@repository/profile';
import {UserRepositoryInterface} from '@repository/user';
import {DatabaseError} from '@repository/error';
import {expect} from 'chai';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {
  UnimplementedDatabaseConnection,
  UnimplementedHasher, UnimplementedLibraries, UnimplementedLoginMailer,
  UnimplementedProfileRepository, UnimplementedRepositories, UnimplementedToken,
  UnimplementedUserRepository,
} from '../../../unimplemented.mock';
import {Profile} from '@model/database/profile';
import {DatabaseConnectionInterface} from '@repository/base';
import {UserProfileRelation} from '@model/database/userProfileRelation';

describe('controller.login', function() {
  class DatabaseException extends Error {
    constructor(message?: string) {
      super(message);
    }
  }

  describe('login', function() {
    it('Database exception', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('whatever');
          return new Promise<User | undefined>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);
      try {
        await loginController.login('whatever', 'whateverPassword');
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof DatabaseException)) {
          throw error;
        }
        expect(error.message).equal('Database error');
      }
    });
    it('Unknown user', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('whatever');
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      try {
        await loginController.login('whatever', 'whateverPassword');
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof AuthenticationError)) {
          throw error;
        }
        expect(error.message).equal('Unknown user');
      }
    });
    it('Wrong password', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('whatever');
          const user = new User;
          user.user_email = 'userEmail';
          user.user_password = 'userPassword';
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }
      }

      class MockHasher extends UnimplementedHasher {
        override verifyPassword(password: string, hash: string): Promise<boolean> {
          expect(password).equal('whateverPassword');
          expect(hash).equal('userPassword');
          return new Promise<boolean>((resolve) => {
            resolve(false);
          });
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        hasher: HasherInterface = new MockHasher;
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);
      try {
        await loginController.login('whatever', 'whateverPassword');
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof AuthenticationError)) {
          throw error;
        }
        expect(error.message).equal('Wrong password');
      }
    });
    it('Successfull login', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('whatever');
          const user = new User;
          user.user_email = 'userEmail';
          user.user_password = 'userPassword';
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }
      }

      class MockHasher extends UnimplementedHasher {
        override verifyPassword(password: string, hash: string): Promise<boolean> {
          expect(password).equal('whateverPassword');
          expect(hash).equal('userPassword');
          return new Promise<boolean>((resolve) => {
            resolve(true);
          });
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal('userEmail');
          const tokenResponse = new TokenResponse;
          tokenResponse.expirationDate = new Date('1995-12-17T03:24:00');
          tokenResponse.token = 'tokenData';
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        hasher = new MockHasher;
        token = new MockToken;
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);

      const loginResponse = await loginController.login('whatever', 'whateverPassword');

      // Use to string to prevent ms errors
      expect(loginResponse.expires.toString()).equal(new Date('1995-12-17T03:24:00').toString());
      expect(loginResponse.token).equal('tokenData');
    });
  });
  describe('register', function() {
    it('missing email', async function() {
      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      const registrationParameters = new RegistrationParameters;

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof RegistrationParametersError)) {
          throw error;
        }
        expect(error.message).equal('Email parameter is required');
      }
    });
    it('get user database exception', async function() {
      const userEmail = 'userEmail';
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(userEmail);
          return new Promise<User | undefined>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = userEmail;

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof DatabaseException)) {
          throw error;
        }
        expect(error.message).equal('Database error');
      }
    });
    it('missing profile id', async function() {
      const userEmail = 'userEmail';
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(userEmail);
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = userEmail;

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof RegistrationParametersError)) {
          throw error;
        }
        expect(error.message).equal('Profile id parameter is required');
      }
    });
    it('Already registered user', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('userEmail');
          return new Promise<User | undefined>((resolve) => {
            resolve(new User);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof RegistrationError)) {
          throw error;
        }
        expect(error.message).equal('User already registered');
      }
    });
    it('isProfileRegistrable Database exception', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = false;

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(registrationParameters.email);
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }

        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof DatabaseException)) {
          throw error;
        }
        expect(error.message).equal('Database error');
      }
    });
    it('Non registrable profile', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = false;

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(registrationParameters.email);
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve, reject) => {
            resolve(profile);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }

        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      try {
        await loginController.register(registrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof RegistrationParametersError)) {
          throw error;
        }
        expect(error.message).equal('The profile choosen is not registrable');
      }
    });
    it('Send registration mail exception', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const hashedPassword = 'abc';

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(registrationParameters.email);
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve, reject) => {
            resolve(profile);
          });
        }
      }

      class MailError extends Error {
        constructor(message?: string) {
          super(message);
        }
      }

      class MockHasher extends UnimplementedHasher {
        override hashPassword(password: string): Promise<string> {
          expect(password).equal(registrationParameters.password);
          return new Promise<string>((resolve) => {
            resolve(hashedPassword);
          });
        }
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override sendRegistrationMail(registrationParameters: RegistrationParameters): Promise<any> {
          expect(registrationParameters.email).equal(registrationParameters.email);
          expect(registrationParameters.profile).equal(registrationParameters.profile);
          expect(registrationParameters.password).equal(hashedPassword);
          return new Promise<any>((resolve, reject) => {
            reject(new MailError('Mail error'));
          });
        }
      }


      class MockLibraries extends UnimplementedLibraries {
        hasher = new MockHasher();
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }

        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration, new MockLibraries);

      try {
        await loginController.register(registrationParameters);
        expect.fail('register must throw an error');
      } catch (error: any) {
        if (!(error instanceof MailError)) {
          throw error;
        }
        expect(error.message).equal('Mail error');
      }
    });
    it('Successfull registration', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const hashedPassword = 'abc';

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(registrationParameters.email);
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve, reject) => {
            resolve(profile);
          });
        }
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override sendRegistrationMail(registrationParameters: RegistrationParameters): Promise<any> {
          expect(registrationParameters.email).equal(registrationParameters.email);
          expect(registrationParameters.profile).equal(registrationParameters.profile);
          expect(registrationParameters.password).equal(hashedPassword);
          return new Promise<any>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockHasher extends UnimplementedHasher {
        override hashPassword(password: string): Promise<string> {
          expect(password).equal(registrationParameters.password);
          return new Promise<string>((resolve) => {
            resolve(hashedPassword);
          });
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        hasher = new MockHasher();
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }

        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration, new MockLibraries);

      await loginController.register(registrationParameters);
    });
  });
  describe('registered', function() {
    it('Exception on decode', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          throw new Error('Decoding error');
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.registered('whatever');
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(RegisteredParametersError);
        expect(error.message).equal('The given token data are not valid');
      }
    });
    it('Decoded bad data', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          return new RegistrationParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.registered('whatever');
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(RegisteredParametersError);
        expect(error.message).equal('The given token data are not valid');
      }
    });
    it('Database exception on getProfileByName', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database error');
      }
    });
    it('Profile not registrable', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = false;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(RegisteredParametersError);
        expect(error.message).equal('The profile choosen is not registrable');
      }
    });
    it('Database exception on insert user', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      let startTransactionCalled = false;
      let commitTransactionCalled = false;
      let rollbackTransactionCalled = false;

      class MockUserRepository extends UnimplementedUserRepository {
        override insertUser(user: User): Promise<number> {
          return new Promise<number>((resolve, reject) => {
            reject(new DatabaseException('Database exception'));
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal(registrationParameters.email);
          const tokenResponse = new TokenResponse;
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        token = new MockToken();
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      class MockDatabaseConnection extends UnimplementedDatabaseConnection {
        override startTransaction(): Promise<void> {
          startTransactionCalled = true;
          return new Promise<void>((resolve) => {
            resolve();
          });
        }

        override commitTransaction(): void {
          commitTransactionCalled = true;
        }

        override rollbackTransaction(): void {
          rollbackTransactionCalled = true;
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get databaseConnection(): DatabaseConnectionInterface {
          return new MockDatabaseConnection;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);


      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database exception');
      }

      expect(startTransactionCalled).true;
      expect(commitTransactionCalled).false;
      expect(rollbackTransactionCalled).true;
    });
    it('User already registered', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      let startTransactionCalled = false;
      let commitTransactionCalled = false;
      let rollbackTransactionCalled = false;

      class MockUserRepository extends UnimplementedUserRepository {
        override insertUser(user: User): Promise<number> {
          return new Promise<number>((resolve, reject) => {
            reject(new DatabaseError('Cannot insert'));
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }
      }

      class MockDatabaseConnection extends UnimplementedDatabaseConnection {
        override startTransaction(): Promise<void> {
          startTransactionCalled = true;
          return new Promise<void>((resolve) => {
            resolve();
          });
        }

        override commitTransaction(): void {
          commitTransactionCalled = true;
        }

        override rollbackTransaction(): void {
          rollbackTransactionCalled = true;
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get databaseConnection(): DatabaseConnectionInterface {
          return new MockDatabaseConnection;
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal(registrationParameters.email);
          const tokenResponse = new TokenResponse;
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        token = new MockToken();
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);


      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(RegisteredError);
        expect(error.message).equal('User already registered');
      }

      expect(startTransactionCalled).true;
      expect(commitTransactionCalled).false;
      expect(rollbackTransactionCalled).true;
    });
    it('Exception on inser user profile relation', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      let startTransactionCalled = false;
      let commitTransactionCalled = false;
      let rollbackTransactionCalled = false;

      const userId = 33;

      class MockUserRepository extends UnimplementedUserRepository {
        override insertUser(user: User): Promise<number> {
          return new Promise<number>((resolve) => {
            resolve(userId);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }

        override insertUserProfileRelation(userProfileRelation: UserProfileRelation): Promise<void> {
          expect(userProfileRelation.user_profile_relation_user_id).equal(userId);
          expect(userProfileRelation.user_profile_relation_profile_id).equal(profile.profile_id);
          return new Promise<void>((resolve, reject) => {
            reject(new DatabaseException('Database exception'));
          });
        }
      }

      class MockDatabaseConnection extends UnimplementedDatabaseConnection {
        override startTransaction(): Promise<void> {
          startTransactionCalled = true;
          return new Promise<void>((resolve) => {
            resolve();
          });
        }

        override commitTransaction(): void {
          commitTransactionCalled = true;
        }

        override rollbackTransaction(): void {
          rollbackTransactionCalled = true;
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get databaseConnection(): DatabaseConnectionInterface {
          return new MockDatabaseConnection;
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal(registrationParameters.email);
          const tokenResponse = new TokenResponse;
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        token = new MockToken();
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);


      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database exception');
      }

      expect(startTransactionCalled).true;
      expect(commitTransactionCalled).false;
      expect(rollbackTransactionCalled).true;
    });
    it('Database error on inser user profile relation', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      let startTransactionCalled = false;
      let commitTransactionCalled = false;
      let rollbackTransactionCalled = false;

      const userId = 33;

      class MockUserRepository extends UnimplementedUserRepository {
        override insertUser(user: User): Promise<number> {
          return new Promise<number>((resolve) => {
            resolve(userId);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }

        override insertUserProfileRelation(userProfileRelation: UserProfileRelation): Promise<void> {
          expect(userProfileRelation.user_profile_relation_user_id).equal(userId);
          expect(userProfileRelation.user_profile_relation_profile_id).equal(profile.profile_id);
          return new Promise<void>((resolve, reject) => {
            reject(new DatabaseError('Database error'));
          });
        }
      }

      class MockDatabaseConnection extends UnimplementedDatabaseConnection {
        override startTransaction(): Promise<void> {
          startTransactionCalled = true;
          return new Promise<void>((resolve) => {
            resolve();
          });
        }

        override commitTransaction(): void {
          commitTransactionCalled = true;
        }

        override rollbackTransaction(): void {
          rollbackTransactionCalled = true;
        }
      }

      const mockDatabaseConnection = new MockDatabaseConnection();

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }

        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get databaseConnection(): DatabaseConnectionInterface {
          return mockDatabaseConnection;
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal(registrationParameters.email);
          const tokenResponse = new TokenResponse;
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        token = new MockToken();
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);


      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);

      try {
        await loginController.registered(encodedRegistrationParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(RegisteredError);
        expect(error.message).equal('Cannot add the given profile to the user');
      }

      expect(startTransactionCalled).true;
      expect(commitTransactionCalled).false;
      expect(rollbackTransactionCalled).true;
    });
    it('Successfully registered', async function() {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'userEmail';
      registrationParameters.password = 'userPassword';
      registrationParameters.profile = 'Visitor';

      const profile: Profile = new Profile;
      profile.profile_id = 26;
      profile.profile_name = registrationParameters.profile;
      profile.profile_can_register = true;

      const encodedRegistrationParameters = 'encodedRegistrationParameters';

      let startTransactionCalled = false;
      let commitTransactionCalled = false;
      let rollbackTransactionCalled = false;

      const userId = 33;

      class MockUserRepository extends UnimplementedUserRepository {
        override insertUser(user: User): Promise<number> {
          return new Promise<number>((resolve) => {
            resolve(userId);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName: string): Promise<Profile> {
          expect(profileName).equal(registrationParameters.profile);
          return new Promise<Profile>((resolve) => {
            resolve(profile);
          });
        }

        override insertUserProfileRelation(userProfileRelation: UserProfileRelation): Promise<void> {
          expect(userProfileRelation.user_profile_relation_user_id).equal(userId);
          expect(userProfileRelation.user_profile_relation_profile_id).equal(profile.profile_id);
          return new Promise<void>((resolve, reject) => {
            resolve();
          });
        }
      }

      class MockDatabaseConnection extends UnimplementedDatabaseConnection {
        override startTransaction(): Promise<void> {
          startTransactionCalled = true;
          return new Promise<void>((resolve) => {
            resolve();
          });
        }

        override commitTransaction(): void {
          commitTransactionCalled = true;
        }

        override rollbackTransaction(): void {
          rollbackTransactionCalled = true;
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
        override get profile(): ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get databaseConnection(): DatabaseConnectionInterface {
          return new MockDatabaseConnection;
        }
      }

      class MockToken extends UnimplementedToken {
        override encodeAndReturnExpiration(payload: string | object): TokenResponse {
          expect(payload).equal(registrationParameters.email);
          const tokenResponse = new TokenResponse;
          return tokenResponse;
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        token = new MockToken();
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeRegistrationParameters(encodedData: string): RegistrationParameters {
          expect(encodedData).equals(encodedRegistrationParameters);
          return registrationParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);


      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);

      await loginController.registered(encodedRegistrationParameters);

      expect(startTransactionCalled).true;
      expect(commitTransactionCalled).true;
      expect(rollbackTransactionCalled).false;
    });
  });
  describe('resetPasswordRequest', function() {
    it('No email', async function() {
      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      try {
        await loginController.resetPasswordRequest(new ResetPasswordRequestParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(ResetPasswordRequestParametersError);
        expect(error.message).equal('Email parameter is required');
      }
    });
    it('Database exception on get user email', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('randomMail@mail.com');
          return new Promise<User | undefined>((resolve, reject) => {
            reject(new DatabaseException('Database exception'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      const resetPasswordParameters = new ResetPasswordRequestParameters;
      resetPasswordParameters.email = 'randomMail@mail.com';

      try {
        await loginController.resetPasswordRequest(resetPasswordParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database exception');
      }
    });
    it('Unknown email', async function() {
      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('randomMail@mail.com');
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      const resetPasswordParameters = new ResetPasswordRequestParameters;
      resetPasswordParameters.email = 'randomMail@mail.com';

      try {
        await loginController.resetPasswordRequest(resetPasswordParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(ResetPasswordRequestError);
        expect(error.message).equal('Cannot find the given user email');
      }
    });
    it('Send mail exception', async function() {
      const user = new User();
      user.user_id = 42;
      user.user_email = 'user@mail.com';

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(user.user_email);
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }
      }

      class MailError extends Error {
        constructor(message?: string) {
          super(message);
        }
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override sendResetPasswordMail(resetPasswordParameters: ResetPasswordRequestParameters): Promise<any> {
          expect(resetPasswordParameters.email).equal(user.user_email);
          return new Promise<any>((resolve, reject) => {
            reject(new MailError('Mail error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      const resetPasswordParameters = new ResetPasswordRequestParameters();
      resetPasswordParameters.email = user.user_email;

      try {
        await loginController.resetPasswordRequest(resetPasswordParameters);
        expect.fail('login must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(MailError);
        expect(error.message).equal('Mail error');
      }
    });
    it('Successfull resetPasswordRequest', async function() {
      const user = new User();
      user.user_id = 42;
      user.user_email = 'user@mail.com';

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(user.user_email);
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }
      }

      class MockLoginMailer extends UnimplementedLoginMailer {
        override sendResetPasswordMail(resetPasswordParameters: ResetPasswordRequestParameters): Promise<any> {
          expect(resetPasswordParameters.email).equal(user.user_email);
          return new Promise<any>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);


      const resetPasswordParameters = new ResetPasswordRequestParameters();
      resetPasswordParameters.email = user.user_email;
      await loginController.resetPasswordRequest(resetPasswordParameters);
    });
  });
  describe('resetPassword', function() {
    it('Exception on decode', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          throw new Error('Decoding error');
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.resetPassword('whatever', 'aNewpassword');
        expect.fail('resetPassword must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(ResetPasswordParametersError);
        expect(error.message).equal('The given token data are not valid');
      }
    });
    it('Decoded bad data', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          return new ResetPasswordRequestParameters;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new UnimplementedRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);


      try {
        await loginController.resetPassword('whatever', 'aNewPassword');
        expect.fail('resetPassword must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(ResetPasswordParametersError);
        expect(error.message).equal('The given token data are not valid');
      }
    });
    it('Exception on get user', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          expect(encodedData).equal('whateverEncoded');
          const decodedData = new ResetPasswordRequestParameters;
          decodedData.email = 'randomMail@mail.com';
          return decodedData;
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('randomMail@mail.com');
          return new Promise<User | undefined>((resolve, reject) => {
            reject(new DatabaseException('Database exception'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);

      try {
        await loginController.resetPassword('whateverEncoded', 'aNewPassword');
        expect.fail('resetPassword must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database exception');
      }
    });
    it('Unknown email', async function() {
      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          expect(encodedData).equal('whateverEncoded');
          const decodedData = new ResetPasswordRequestParameters;
          decodedData.email = 'randomMail@mail.com';
          return decodedData;
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('randomMail@mail.com');
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);


      try {
        await loginController.resetPassword('whateverEncoded', 'aNewPassword');
        expect.fail('resetPassword must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(ResetPasswordError);
        expect(error.message).equal('Cannot find the given user email');
      }
    });
    it('Exception on update password', async function() {
      const user = new User;
      user.user_email = 'randomMail@mail.com';
      user.user_id = 42;
      user.user_password = 'apassword';

      const newPassword = 'aNewPassword';

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          expect(encodedData).equal('encodedData');
          const decodedData = new ResetPasswordRequestParameters;
          decodedData.email = user.user_email;
          return decodedData;
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(user.user_email);
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }

        updatePassword(userId: number, password: string): Promise<void> {
          expect(userId).equal(user.user_id);
          expect(password).equal('abcd');
          return new Promise<void>((resolve, reject) => {
            reject(new DatabaseException('Database exception'));
          });
        }
      }

      class MockHasher extends UnimplementedHasher {
        override hashPassword(password: string): Promise<string> {
          expect(password).equal(newPassword);
          return new Promise<string>((resolve) => {
            resolve('abcd');
          });
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        hasher = new MockHasher;
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new UnimplementedLibraries);


      try {
        await loginController.resetPassword('encodedData', 'aNewPassword');
        expect.fail('resetPassword must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database exception');
      }
    });
    it('Successfull resetPassword', async function() {
      const user = new User;
      user.user_email = 'randomMail@mail.com';
      user.user_id = 42;
      user.user_password = 'apassword';

      const newPassword = 'aNewPassword';

      class MockLoginMailer extends UnimplementedLoginMailer {
        override decodeResetPasswordParameters(encodedData: string): ResetPasswordRequestParameters {
          expect(encodedData).equal('encodedData');
          const decodedData = new ResetPasswordRequestParameters;
          decodedData.email = user.user_email;
          return decodedData;
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal(user.user_email);
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }

        updatePassword(userId: number, password: string): Promise<void> {
          expect(userId).equal(user.user_id);
          expect(password).equal('abcd');
          return new Promise<void>((resolve) => {
            resolve();
          });
        }
      }

      class MockHasher extends UnimplementedHasher {
        override hashPassword(password: string): Promise<string> {
          expect(password).equal(newPassword);
          return new Promise<string>((resolve) => {
            resolve('abcd');
          });
        }
      }

      class MockLibraries extends UnimplementedLibraries {
        hasher = new MockHasher;
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user(): UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const loginController = new LoginController(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries, new MockRepositories);

      class MockLoginMailerFacade extends LoginMailerFacade {
        override get loginMailer(): LoginMailerInterface {
          return new MockLoginMailer;
        }
      }

      loginController.loginMailerFacade = new MockLoginMailerFacade(new ConfigurationTest().loginMailerConfiguration,
          new MockLibraries);


      await loginController.resetPassword('encodedData', newPassword);
    });
  });
});
