import {AuthorizationController} from '@controller/authorization/authorization';
import {AuthorizationError} from '@controller/authorization/error';
import {Authorization} from '@model/database/authorization';
import {Profile} from '@model/database/profile';
import {User} from '@model/database/user';
import {AuthorizationRepositoryInterface} from '@repository/authorization';
import {ProfileRepositoryInterface} from '@repository/profile';
import {UserRepositoryInterface} from '@repository/user';
import {expect} from 'chai';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {UnimplementedAuthorizationRepository, UnimplementedProfileRepository, UnimplementedRepositories,
  UnimplementedToken, UnimplementedUserRepository} from '../../../unimplemented.mock';


describe('controller.authorization', function() {
  class DatabaseException extends Error {
    constructor(message?: string) {
      super(message);
    }
  }
  describe('loggedInUser', function() {
    it('token decoding error', async function() {
      class MockToken extends UnimplementedToken {
        decode(payload: string): string | object {
          expect(payload).equal('tokenData');
          throw new Error('Decoding error');
        }
      }

      const authorizationController = new AuthorizationController(new UnimplementedRepositories, new MockToken);

      try {
        await authorizationController.loggedInUser('tokenData');
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(AuthorizationError);
        expect(error.message).equal('Decoding error');
      }
    });
    it('token invalid data', async function() {
      class MockToken extends UnimplementedToken {
        decode(payload: string): string | object {
          expect(payload).equal('tokenData');
          return {'something': 'wrong'};
        }
      }

      const authorizationController = new AuthorizationController(new UnimplementedRepositories, new MockToken);

      try {
        await authorizationController.loggedInUser('tokenData');
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(AuthorizationError);
        expect(error.message).equal('Invalid authorization');
      }
    });
    it('database exception', async function() {
      class MockToken extends UnimplementedToken {
        decode(payload: string): string | object {
          expect(payload).equal('tokenData');
          return 'mockEmail';
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('mockEmail');
          return new Promise<User | undefined>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user() : UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new MockToken);

      try {
        await authorizationController.loggedInUser('tokenData');
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database error');
      }
    });
    it('unknown user', async function() {
      class MockToken extends UnimplementedToken {
        decode(payload: string): string | object {
          expect(payload).equal('tokenData');
          return 'mockEmail';
        }
      }

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('mockEmail');
          return new Promise<User | undefined>((resolve) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user() : UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new MockToken);

      try {
        await authorizationController.loggedInUser('tokenData');
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(AuthorizationError);
        expect(error.message).equal('Unknown user');
      }
    });
    it('known user', async function() {
      class MockToken extends UnimplementedToken {
        override decode(payload: string): string | object {
          expect(payload).equal('tokenData');
          return 'mockEmail';
        }
      }

      const user = new User;
      user.user_email = 'mockEmail';
      user.user_password = 'patate';

      class MockUserRepository extends UnimplementedUserRepository {
        override getUserByEmail(email: string): Promise<User | undefined> {
          expect(email).equal('mockEmail');
          return new Promise<User | undefined>((resolve) => {
            resolve(user);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get user() : UserRepositoryInterface {
          return new MockUserRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new MockToken);

      const returnedUser = await authorizationController.loggedInUser('tokenData');
      expect(returnedUser).equal(user);
    });
  });

  describe('getAuthorizationsOfProfile', function() {
    it('database exception getProfileByName', async function() {
      const expectedProfileName = 'profileName';

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName : string) : Promise<Profile | undefined> {
          expect(profileName).equal(expectedProfileName);
          return new Promise<Profile>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      try {
        await authorizationController.getAuthorizationsOfProfile(expectedProfileName);
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database error');
      }
    });
    it('Unknown profile', async function() {
      const expectedProfileName = 'profileName';

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName : string) : Promise<Profile | undefined> {
          expect(profileName).equal(expectedProfileName);
          return new Promise<Profile | undefined>((resolve, reject) => {
            resolve(undefined);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      try {
        await authorizationController.getAuthorizationsOfProfile(expectedProfileName);
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(AuthorizationError);
        expect(error.message).equal('Unknown profile');
      }
    });
    it('database exception getAuthorizationsOfProfile', async function() {
      const expectedProfileName = 'profileName';

      const returnProfile : Profile = new Profile;
      returnProfile.profile_id = 42;
      returnProfile.profile_name = 'Administrator';
      returnProfile.profile_description = 'Administrator of the website, able to administrate users';
      returnProfile.profile_can_register = true;

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName : string) : Promise<Profile | undefined> {
          expect(profileName).equal(expectedProfileName);
          return new Promise<Profile | undefined>((resolve, reject) => {
            resolve(returnProfile);
          });
        }
      }

      class MockAuthorizationRepository extends UnimplementedAuthorizationRepository {
        override getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]> {
          expect(profileId).equal(returnProfile.profile_id);
          return new Promise<Authorization[]>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get authorization() : AuthorizationRepositoryInterface {
          return new MockAuthorizationRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      try {
        await authorizationController.getAuthorizationsOfProfile(expectedProfileName);
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database error');
      }
    });
    it('Success', async function() {
      const expectedProfileName = 'profileName';

      const returnProfile : Profile = new Profile;
      returnProfile.profile_id = 42;
      returnProfile.profile_name = 'Administrator';
      returnProfile.profile_description = 'Administrator of the website, able to administrate users';
      returnProfile.profile_can_register = true;

      const expectedAuthorizations : Authorization[] = [];

      const authorization0 = new Authorization;
      authorization0.authorization_id = 1;
      authorization0.authorization_name = 'CanAdministrateUsers';
      authorization0.authorization_description = 'Can create, update, remove user and give them access authorization';
      expectedAuthorizations.push(authorization0);

      const authorization1 = new Authorization;
      authorization1.authorization_id = 2;
      authorization1.authorization_name = 'CanSeeHisPlanning';
      authorization1.authorization_description = 'Can consult his planning';
      expectedAuthorizations.push(authorization1);

      class MockAuthorizationRepository extends UnimplementedAuthorizationRepository {
        override getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]> {
          expect(profileId).equal(returnProfile.profile_id);
          return new Promise<Authorization[]>((resolve) => {
            resolve(expectedAuthorizations);
          });
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfileByName(profileName : string) : Promise<Profile | undefined> {
          expect(profileName).equal(expectedProfileName);
          return new Promise<Profile | undefined>((resolve, reject) => {
            resolve(returnProfile);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get authorization() : AuthorizationRepositoryInterface {
          return new MockAuthorizationRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      const authorizationResponses : AuthorizationResponse[] =
      await authorizationController.getAuthorizationsOfProfile(expectedProfileName);

      expect(authorizationResponses.length).equal(2);

      const authorizationResponse : AuthorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = authorization0.authorization_name;
      authorizationResponse.description = authorization0.authorization_description;

      let actualAuthorizationResponse : AuthorizationResponse = authorizationResponses[0];

      expect(actualAuthorizationResponse).to.eql(authorizationResponse);

      authorizationResponse.name = authorization1.authorization_name;
      authorizationResponse.description = authorization1.authorization_description;

      actualAuthorizationResponse = authorizationResponses[1];

      expect(actualAuthorizationResponse).to.eql(authorizationResponse);
    });
  });
  describe('getAuthorizationsOfUser', function() {
    it('Database exception on get profiles', async function() {
      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfilesOfUser(userId : number) : Promise<Profile[]> {
          return new Promise<Profile[]>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      try {
        await authorizationController.getAuthorizationsOfUser(42);
        expect.fail('');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseException);
        expect(error.message).equal('Database error');
      }
    });
    it('Empty profiles', async function() {
      const profiles : Profile[] = [];
      const sentUserId = 53;

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfilesOfUser(userId : number) : Promise<Profile[]> {
          expect(userId).equal(sentUserId);
          return new Promise<Profile[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      const result : AuthorizationResponse[] = await authorizationController.getAuthorizationsOfUser(sentUserId);

      expect(result).to.eql([]);
    });
    it('Multiple profiles', async function() {
      const profiles : Profile[] = [];

      let profile = new Profile;
      profile.profile_id = 42;
      profile.profile_name = 'Administrator';
      profile.profile_description = 'Administrator of the website, able to administrate users';
      profile.profile_can_register = true;

      profiles.push(profile);

      profile = new Profile;
      profile.profile_id = 43;
      profile.profile_name = 'Vilain';
      profile.profile_description = 'A vilain who can edit his profile and receive missions';
      profile.profile_can_register = false;

      profiles.push(profile);

      const authorizations42 : Authorization[] = [];

      let authorization = new Authorization;
      authorization.authorization_name = 'CanAdministrate';
      authorization.authorization_description = 'Can administrate users';

      authorizations42.push(authorization);

      const authorizations43 : Authorization[] = [];

      authorization = new Authorization;
      authorization.authorization_name = 'CanEditHisProfile';
      authorization.authorization_description = 'Can edit is profile';

      authorizations43.push(authorization);

      authorization = new Authorization;
      authorization.authorization_name = 'CanReceiveMissions';
      authorization.authorization_description = 'Can receive missions';

      authorizations43.push(authorization);

      const authorizationResponses : AuthorizationResponse[] = [];

      let authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = authorizations42[0].authorization_name;
      authorizationResponse.description = authorizations42[0].authorization_description;

      authorizationResponses.push(authorizationResponse);

      authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = authorizations43[0].authorization_name;
      authorizationResponse.description = authorizations43[0].authorization_description;

      authorizationResponses.push(authorizationResponse);

      authorizationResponse = new AuthorizationResponse;
      authorizationResponse.name = authorizations43[1].authorization_name;
      authorizationResponse.description = authorizations43[1].authorization_description;

      authorizationResponses.push(authorizationResponse);

      const sentUserId = 53;

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfilesOfUser(userId : number) : Promise<Profile[]> {
          expect(userId).equal(sentUserId);
          return new Promise<Profile[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      class MockAuthorizationRepository extends UnimplementedAuthorizationRepository {
        override getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]> {
          return new Promise<Authorization[]>((resolve) => {
            if (profileId == profiles[0].profile_id) {
              resolve(authorizations42);
            } else if (profileId == profiles[1].profile_id) {
              resolve(authorizations43);
            } else {
              expect.fail('Unexpected profile id');
            }
          });
        }
      }

      class MockRepositories extends UnimplementedRepositories {
        override get profile() : ProfileRepositoryInterface {
          return new MockProfileRepository;
        }

        override get authorization() : AuthorizationRepositoryInterface {
          return new MockAuthorizationRepository;
        }
      }

      const authorizationController = new AuthorizationController(new MockRepositories, new UnimplementedToken);

      const result : AuthorizationResponse[] = await authorizationController.getAuthorizationsOfUser(sentUserId);

      expect(result).to.eql(authorizationResponses);
    });
  });
});
