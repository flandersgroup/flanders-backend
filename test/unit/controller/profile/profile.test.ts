import {ProfileController} from '@controller/profile/profile';
import {Profile} from '@model/database/profile';
import {expect} from 'chai';
import {ProfileResponse} from 'common/server/response/profile';
import {UnimplementedProfileRepository} from '../../../unimplemented.mock';


describe('controller.profile', function() {
  describe('getRegistrableProfiles', function() {
    it('Database exception', async function() {
      class DatabaseException extends Error {
        constructor(message?: string) {
          super(message);
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getRegistrableProfiles(): Promise<Profile[]> {
          return new Promise<Profile[]>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      const profileController = new ProfileController(new MockProfileRepository);

      try {
        await profileController.getRegistrableProfiles();
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof DatabaseException)) {
          throw error;
        }
        expect(error.message).equal('Database error');
      }
    });
    it('Success', async function() {
      const profiles : Profile[] = [];

      const profile = new Profile;
      profile.profile_id = 1;
      profile.profile_name = 'Profile1';
      profile.profile_description = 'Profile description 1';
      profile.profile_can_register = true;

      profiles.push(profile);

      profile.profile_id = 3;
      profile.profile_name = 'Profile3';
      profile.profile_description = 'Profile description 3';
      profile.profile_can_register = true;

      profiles.push(profile);

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getRegistrableProfiles(): Promise<Profile[]> {
          return new Promise<Profile[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      const profileController = new ProfileController(new MockProfileRepository);

      const profileResponses : ProfileResponse[] = [];

      const profileResponse = new ProfileResponse;

      profileResponse.name = profiles[0].profile_name;
      profileResponse.description = profiles[0].profile_description;

      profileResponses.push(profileResponse);


      profileResponse.name = profiles[1].profile_name;
      profileResponse.description = profiles[1].profile_description;

      profileResponses.push(profileResponse);

      expect(await profileController.getRegistrableProfiles()).to.eql(profileResponses);
    });
  });
  describe('getProfilesOfUser', function() {
    it('Database exception', async function() {
      const expectedUserId = 21;

      class DatabaseException extends Error {
        constructor(message?: string) {
          super(message);
        }
      }

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfilesOfUser(userId : number) : Promise<Profile[]> {
          expect(expectedUserId).equal(userId);
          return new Promise<Profile[]>((resolve, reject) => {
            reject(new DatabaseException('Database error'));
          });
        }
      }

      const profileController = new ProfileController(new MockProfileRepository);

      try {
        await profileController.getProfilesOfUser(expectedUserId);
        expect.fail('login must throw an error');
      } catch (error: any) {
        if (!(error instanceof DatabaseException)) {
          throw error;
        }
        expect(error.message).equal('Database error');
      }
    });
    it('Success', async function() {
      const expectedUserId = 21;

      const profiles : Profile[] = [];

      const profile : Profile = new Profile;
      profile.profile_id = 42;
      profile.profile_name = 'Administrator';
      profile.profile_description = 'Administrator of the website, able to administrate users';
      profile.profile_can_register = true;

      profiles.push(profile);

      profile.profile_id = 43;
      profile.profile_name = 'Vilain';
      profile.profile_description = 'A vilain who can edit his profile and receive missions';
      profile.profile_can_register = false;

      profiles.push(profile);

      const profileResponses : ProfileResponse[] = [];

      const profileResponse : ProfileResponse = new ProfileResponse;

      profileResponse.name = 'Administrator';
      profileResponse.description = 'Administrator of the website, able to administrate users';

      profileResponses.push(profileResponse);

      profileResponse.name = 'Vilain';
      profileResponse.description = 'A vilain who can edit his profile and receive missions';

      profileResponses.push(profileResponse);

      class MockProfileRepository extends UnimplementedProfileRepository {
        override getProfilesOfUser(userId : number) : Promise<Profile[]> {
          expect(expectedUserId).equal(userId);
          return new Promise<Profile[]>((resolve) => {
            resolve(profiles);
          });
        }
      }

      const profileController = new ProfileController(new MockProfileRepository);

      const actualProfiles = await profileController.getProfilesOfUser(expectedUserId);
      expect(actualProfiles).to.eql(profileResponses);
    });
  });
});
