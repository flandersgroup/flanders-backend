import {ConfigurationTest} from '@configuration/config.test';
import {UserRepository} from '@repository/user/user';
import {expect} from 'chai';
import {User} from '@model/database/user';
import {DatabaseError} from '@repository/error';
import {DatabaseConnection} from '@repository/base/databaseConnection';

const configuration = new ConfigurationTest();
const database = new DatabaseConnection(configuration.databaseConfiguration);
const userRepository = new UserRepository(database);

describe('repository.user', function() {
  describe('getUserByEmail', function() {
    it('success', async function() {
      const user = await userRepository.getUserByEmail('admin.profile1@test.com');
      if (user !== undefined) {
        expect(user.user_email).equal('admin.profile1@test.com');
        expect(user.user_id).equal(1);
        expect(user.user_enabled).equal(true);
        expect(user.user_first_name).equal('admin');
        expect(user.user_last_name).equal('user');
        expect(user.user_password).equal('$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O');
      } else {
        expect.fail('User is undefined');
      }
    });
    it('unkown user', async function() {
      const user = await userRepository.getUserByEmail('whatever');
      expect(user).undefined;
    });
  });
  describe('insertUser', function() {
    beforeEach(async () => {
      await userRepository.databaseConnection.startTransaction();
    });
    it('non existing user', async function() {
      const user = new User;
      user.user_email = 'userMailToInsert@test.com';
      user.user_first_name = 'userToInsert';
      user.user_last_name = 'ForTest';
      user.user_password = 'aPassword';

      let userFromDb = await userRepository.getUserByEmail(user.user_email);
      expect(userFromDb).undefined;

      await userRepository.insertUser(user);

      userFromDb = await userRepository.getUserByEmail(user.user_email);
      expect(userFromDb).not.undefined;
      if (userFromDb) {
        user.user_id = userFromDb.user_id;
        console.log('Index : ' + user.user_id);
      }
      expect(userFromDb).to.eql(user);
    });
    it('existing user', async function() {
      const user = new User;
      user.user_email = 'userMailToInsert@test.com';
      user.user_first_name = 'userToInsert';
      user.user_last_name = 'ForTest';
      user.user_password = 'aPassword';

      let userFromDb = await userRepository.getUserByEmail(user.user_email);
      expect(userFromDb).undefined;

      await userRepository.insertUser(user);

      userFromDb = await userRepository.getUserByEmail(user.user_email);

      // Insert twice
      try {
        await userRepository.insertUser(user);
        expect.fail('Insert twice must throw an error');
      } catch (error: any) {
        expect(error).instanceOf(DatabaseError);
      }
    });
    afterEach(() => {
      userRepository.databaseConnection.rollbackTransaction();
    });
  });
  describe('updatePassword', function() {
    beforeEach(async () => {
      await userRepository.databaseConnection.startTransaction();
    });

    it('success', async function() {
      let user = await userRepository.getUserByEmail('admin.profile1@test.com');
      if (user !== undefined && user.user_id != undefined) {
        const newPassword = 'ANewPassword';
        expect(user.user_password).not.equal(newPassword);
        await userRepository.updatePassword(user.user_id, newPassword);
        user = await userRepository.getUserByEmail('admin.profile1@test.com');
        if (user != undefined) {
          expect(user.user_password).equal(newPassword);
        } else {
          expect.fail('Unexpected undefined user');
        }
      } else {
        expect.fail('Unexpected undefined user');
      }
    });
    it('unknown user', async function() {
      await userRepository.updatePassword(42, 'a new password');
    });

    afterEach(async () => {
      userRepository.databaseConnection.rollbackTransaction();
    });
  });
});
