import {ConfigurationTest} from '@configuration/config.test';
import {DatabaseConnection} from '@repository/base/databaseConnection';
import {expect} from 'chai';

describe('repository.base', function() {
  const configuration = new ConfigurationTest;
  const tableName = 'test';

  before(async () => {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.connection.schema.createTable(tableName, function(table) {
      table.string('name');
    });
  });

  it('connection', async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.connection.insert({'name': 'toto'}).into(tableName);
    const row = await databaseConnection.connection.queryBuilder().table(tableName).select();
    expect(row).to.eql([{'name': 'toto'}]);
  });

  it('commit transaction', async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.startTransaction();
    await databaseConnection.connection.insert({'name': 'toto'}).into(tableName);
    databaseConnection.commitTransaction();
    const row = await databaseConnection.connection.queryBuilder().table(tableName).select();
    expect(row).to.eql([{'name': 'toto'}]);
  });

  it('rollback transaction', async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.startTransaction();
    await databaseConnection.connection.insert({'name': 'toto'}).into(tableName);
    databaseConnection.rollbackTransaction();
    const row = await databaseConnection.connection.queryBuilder().table(tableName).select();
    expect(row).to.eql([]);
  });

  it('call commit and not start', async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.connection.insert({'name': 'toto'}).into(tableName);
    databaseConnection.commitTransaction();
    const row = await databaseConnection.connection.queryBuilder().table(tableName).select();
    expect(row).to.eql([{'name': 'toto'}]);
  });

  it('call roolback and not start', async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.connection.insert({'name': 'toto'}).into(tableName);
    databaseConnection.rollbackTransaction();
    const row = await databaseConnection.connection.queryBuilder().table(tableName).select();
    expect(row).to.eql([{'name': 'toto'}]);
  });

  afterEach(async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);

    await databaseConnection.connection(tableName).del().where('name', 'toto');
  });

  after(async function() {
    const databaseConnection = new DatabaseConnection(configuration.databaseConfiguration);
    await databaseConnection.connection.schema.dropTable(tableName);
  });
});
