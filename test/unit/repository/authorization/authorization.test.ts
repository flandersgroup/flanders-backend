import {ConfigurationTest} from '@configuration/config.test';
import {expect} from 'chai';
import {AuthorizationRepository} from '@repository/authorization/authorization';
import {Authorization} from '@model/database/authorization';
import {DatabaseConnection} from '@repository/base/databaseConnection';

const configuration = new ConfigurationTest();
const database = new DatabaseConnection(configuration.databaseConfiguration);
const authorizationRepository = new AuthorizationRepository(database);

describe('repository.authorization', function() {
  it('getAuthorizations', async function() {
    const authorizations = await authorizationRepository.getAuthorizations();

    const expectedAuthorizations : Authorization[] = [];

    let authorization = new Authorization;
    authorization.authorization_id = 1;
    authorization.authorization_name = 'CanAdministrateUsers';
    authorization.authorization_description = 'Can create, update, remove user and give them access authorization';
    expectedAuthorizations.push(authorization);

    authorization = new Authorization;
    authorization.authorization_id = 2;
    authorization.authorization_name = 'CanSeeHisPlanning';
    authorization.authorization_description = 'Can consult his planning';
    expectedAuthorizations.push(authorization);

    authorization = new Authorization;
    authorization.authorization_id = 3;
    authorization.authorization_name = 'CanSeeHisAssignedMissions';
    authorization.authorization_description = 'Can consult the missions that are assigned to him';
    expectedAuthorizations.push(authorization);

    authorization = new Authorization;
    authorization.authorization_id = 4;
    authorization.authorization_name = 'CanSeeHisSubmittedMissions';
    authorization.authorization_description = 'Can consult the missions he submitted';
    expectedAuthorizations.push(authorization);

    authorization = new Authorization;
    authorization.authorization_id = 5;
    authorization.authorization_name = 'CanSubmitMission';
    authorization.authorization_description = 'Can submit mission';
    expectedAuthorizations.push(authorization);

    authorization = new Authorization;
    authorization.authorization_id = 6;
    authorization.authorization_name = 'CanSeeHisProfile';
    authorization.authorization_description = 'Can see his candidate profile';
    expectedAuthorizations.push(authorization);

    expect(authorizations).eql(expectedAuthorizations);
  });
  describe('getAuthorizationsOfProfile', function() {
    it('known profile', async function() {
      const authorizations = await authorizationRepository.getAuthorizationsOfProfile(2);

      const expectedAuthorizations : Authorization[] = [];
      let authorization = new Authorization;
      authorization.authorization_id = 2;
      authorization.authorization_name = 'CanSeeHisPlanning';
      authorization.authorization_description = 'Can consult his planning';
      expectedAuthorizations.push(authorization);

      authorization = new Authorization;
      authorization.authorization_id = 3;
      authorization.authorization_name = 'CanSeeHisAssignedMissions';
      authorization.authorization_description = 'Can consult the missions that are assigned to him';
      expectedAuthorizations.push(authorization);

      expect(authorizations).eql(expectedAuthorizations);
    });
    it('unknwon profile', async function() {
      const authorizations = await authorizationRepository.getAuthorizationsOfProfile(42);

      const expectedAuthorizations : Authorization[] = [];
      expect(authorizations).eql(expectedAuthorizations);
    });
  });
});
