import {ConfigurationTest} from '@configuration/config.test';
import {ProfileRepository} from '@repository/profile/profile';
import {expect} from 'chai';
import {Profile} from '@model/database/profile';
import {UserProfileRelation} from '@model/database/userProfileRelation';
import {DatabaseError} from '@repository/error';
import {DatabaseConnection} from '@repository/base/databaseConnection';

const configuration = new ConfigurationTest();
const database = new DatabaseConnection(configuration.databaseConfiguration);
const profileRepository = new ProfileRepository(database);

describe('repository.profile', function() {
  it('getRegistrableProfiles', async function() {
    const profiles = await profileRepository.getRegistrableProfiles();

    expect(profiles.length).equal(3);
    for (const profile of profiles) {
      if (profile.profile_id === 3) {
        expect(profile.profile_name).equals('Visitor');
        expect(profile.profile_description).equals('A visitor');
        expect(profile.profile_can_register).true;
      } else if (profile.profile_id === 4) {
        expect(profile.profile_name).equals('Hero');
        expect(profile.profile_description).equals('A hero who can submit mission for vilains');
        expect(profile.profile_can_register).true;
      } else if (profile.profile_id === 5) {
        expect(profile.profile_name).equals('Candidate');
        expect(profile.profile_description).equals('A candidate to become a vilain');
        expect(profile.profile_can_register).true;
      } else {
        expect.fail('Unexpected profile id ');
      }
    }
  });

  describe('isProfileRegistrable', function() {
    it('registrable profile', async function() {
      const isRegistrable = await profileRepository.isProfileRegistrable(3);
      expect(isRegistrable).true;
    });
    it('non registrable profile', async function() {
      const isRegistrable = await profileRepository.isProfileRegistrable(1);
      expect(isRegistrable).false;
    });
    it('unknown profile', async function() {
      const isRegistrable = await profileRepository.isProfileRegistrable(490);
      expect(isRegistrable).undefined;
    });
  });

  describe('getProfileByName', function() {
    it('Known profile', async function() {
      const profile : Profile = new Profile;
      profile.profile_id = 2;
      profile.profile_name = 'Vilain';
      profile.profile_description = 'A vilain who can edit his profile and receive missions';
      profile.profile_can_register = false;

      const returnedProfile = await profileRepository.getProfileByName(profile.profile_name);

      expect(returnedProfile).to.eql(profile);
    });
    it('Unknown profile', async function() {
      const returnedProfile = await profileRepository.getProfileByName('Whatever');

      expect(returnedProfile).undefined;
    });
  });

  describe('getProfilesOfUser', function() {
    it('User with multiple profiles', async function() {
      const profiles = await profileRepository.getProfilesOfUser(1);

      expect(profiles.length).equal(2);
      for (const profile of profiles) {
        if (profile.profile_id === 1) {
          expect(profile.profile_name).equals('Administrator');
          expect(profile.profile_description).equals('Administrator of the website, able to administrate users');
          expect(profile.profile_can_register).false;
        } else if (profile.profile_id === 2) {
          expect(profile.profile_name).equals('Vilain');
          expect(profile.profile_description).equals('A vilain who can edit his profile and receive missions');
          expect(profile.profile_can_register).false;
        } else {
          expect.fail('Unexpected profile id ');
        }
      }
    });
    it('User with one profile', async function() {
      const profiles = await profileRepository.getProfilesOfUser(5);

      expect(profiles.length).equal(1);
      for (const profile of profiles) {
        if (profile.profile_id === 3) {
          expect(profile.profile_name).equals('Visitor');
          expect(profile.profile_description).equals('A visitor');
          expect(profile.profile_can_register).true;
        } else {
          expect.fail('Unexpected profile id ');
        }
      }
    });
    it('Unknown user', async function() {
      const profiles = await profileRepository.getProfilesOfUser(42);

      expect(profiles.length).equal(0);
    });
  });

  describe('insertUserProfileRelation', function() {
    beforeEach(async () => {
      await profileRepository.databaseConnection.startTransaction();
    });
    it('Success', async function() {
      const userProfileRelation : UserProfileRelation = new UserProfileRelation;
      userProfileRelation.user_profile_relation_user_id = 2;
      userProfileRelation.user_profile_relation_profile_id = 3;
      await profileRepository.insertUserProfileRelation(userProfileRelation);

      const profiles = await profileRepository.getProfilesOfUser(2);
      let found : boolean = false;

      for (const profile of profiles) {
        if (profile.profile_id==3) {
          found = true;
        }
      }

      expect(found).true;
    });
    it('Unknown user', async function() {
      const userProfileRelation : UserProfileRelation = new UserProfileRelation;
      userProfileRelation.user_profile_relation_user_id = 42;
      userProfileRelation.user_profile_relation_profile_id = 3;

      try {
        await profileRepository.insertUserProfileRelation(userProfileRelation);
        expect.fail();
      } catch (error:any) {
        expect(error).not.instanceOf(DatabaseError);
      }
    });
    it('Unknown profile', async function() {
      const userProfileRelation : UserProfileRelation = new UserProfileRelation;
      userProfileRelation.user_profile_relation_user_id = 2;
      userProfileRelation.user_profile_relation_profile_id = 42;
      try {
        await profileRepository.insertUserProfileRelation(userProfileRelation);
        expect.fail();
      } catch (error:any) {
        expect(error).not.instanceOf(DatabaseError);
      }
    });
    it('Relation already exists', async function() {
      const userProfileRelation : UserProfileRelation = new UserProfileRelation;
      userProfileRelation.user_profile_relation_user_id = 1;
      userProfileRelation.user_profile_relation_profile_id = 1;
      try {
        await profileRepository.insertUserProfileRelation(userProfileRelation);
        expect.fail();
      } catch (error:any) {
        expect(error).instanceOf(DatabaseError);
      }
    });
    afterEach(async () => {
      profileRepository.databaseConnection.rollbackTransaction();
    });
  });
});
