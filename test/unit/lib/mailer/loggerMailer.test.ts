import {Logger} from '@lib/logger';
import {LoggerMailer} from '@lib/mailer/loggerMailer';
import {expect} from 'chai';

describe('lib.mail.loggerMailer', function() {
  describe('send mail', function() {
    const from = 'fromValue';
    const to = 'toValue';
    const subject = 'subjectvalue';
    const text = 'textValue';
    const html = 'htmlValue';

    const logs = ['Received mail to send', 'From : ' + from, 'To : ' + to, 'Subject : ' + subject, 'Text : ' + text, 'Html : ' + html];

    let logIndex = 0;

    class MockMailer implements Logger {
      error(message: any): Logger {
        throw new Error('Not implemented');
      }
      warn(message: any): Logger {
        throw new Error('Not implemented');
      }
      info(message: any): Logger {
        expect(message).equals(logs[logIndex]);
        logIndex++;
        return this;
      }
      debug(message: any): Logger {
        throw new Error('Not implemented');
      }
      silly(message: any): Logger {
        throw new Error('Not implemented');
      }

      close(): Logger {
        throw new Error('Not implemented');
      }
    }

    const logMailler = new LoggerMailer(new MockMailer);
    logMailler.from = from;
    logMailler.to = to;
    logMailler.subject = subject;
    logMailler.text = text;
    logMailler.html = html;

    logMailler.sendMail();
  });
});
