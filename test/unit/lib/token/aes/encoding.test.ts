import {ConfigurationTest} from '@configuration/config.test';
import {AesEncoding} from '@lib/token/aes/encoding';
import {DecodingError} from '@lib/token/aes/error';
import {expect} from 'chai';

describe('lib.token.aes.encoding', function() {
  describe('encode then decode', function() {
    it('valid data', function() {
      const aes = new AesEncoding(new ConfigurationTest().tokenConfiguration.key);

      const stringToEncode = 'String to encode long enought to see if error occurs';
      const encrypted = aes.encode(stringToEncode);

      const decrypted = aes.decode(encrypted);

      expect(decrypted).equal(stringToEncode);
    });
    it('invalid format', function() {
      const aes = new AesEncoding(new ConfigurationTest().tokenConfiguration.key);

      try {
        aes.decode('whatever');
        expect.fail('An error must be thrown');
      } catch (error) {
        expect(error).instanceOf(DecodingError);
      }
    });
    it('invalid data', function() {
      const aes = new AesEncoding(new ConfigurationTest().tokenConfiguration.key);

      try {
        aes.decode('whatever.data');
        expect.fail('An error must be thrown');
      } catch (error) {
        expect(error).instanceOf(DecodingError);
      }
    });
  });
});
