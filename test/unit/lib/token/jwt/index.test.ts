import {ConfigurationTest} from '@configuration/config.test';
import {JwtToken} from '@lib/token/jwt';
import {expect} from 'chai';
import {JsonWebTokenError, TokenExpiredError} from 'jsonwebtoken';

const configurationTest = new ConfigurationTest;

describe('lib.token.jwt.index', function() {
  describe('encode then decode', function() {
    it('valid data string', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      const stringToEncode = 'String to encode';
      const encrypted = aesToken.encode(stringToEncode);

      const decrypted = aesToken.decode(encrypted);

      expect(decrypted).equal(stringToEncode);
    });
    it('valid data object', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      class TestClass {
        data: string = 'some data';
      }
      const dataToEncode = new TestClass();
      const encrypted = aesToken.encode(dataToEncode);

      const decrypted = aesToken.decode(encrypted);

      expect(decrypted).is.not.string;
      expect(decrypted).is.not.undefined;
      expect((decrypted as TestClass).data).equal(dataToEncode.data);
    });
    it('invalid format', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      try {
        aesToken.decode('whatever');
        expect.fail('An error must be thrown');
      } catch (error : any) {
        expect(error).instanceOf(JsonWebTokenError);
      }
    });
    it('expired token', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);
      aesToken.expiresIn = 0;

      const stringToEncode = 'String to encode';
      const encrypted = aesToken.encode(stringToEncode);

      try {
        aesToken.decode(encrypted);
        expect.fail('An error must be thrown');
      } catch (error : any) {
        expect(error).instanceOf(TokenExpiredError);
      }
    });
  });
  describe('encodeAndReturnExpiration', function() {
    it('valid data string', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      const stringToEncode = 'String to encode';
      const encrypted = aesToken.encodeAndReturnExpiration(stringToEncode);

      const expirationDate = encrypted.expirationDate;
      const maxExpirationDate = new Date(Date.now() + (aesToken.expiresIn + 1) * 1000);
      const minExpirationDate = new Date(Date.now() + (aesToken.expiresIn - 1) * 1000);

      expect(expirationDate).above(minExpirationDate).below(maxExpirationDate);

      const decrypted = aesToken.decode(encrypted.token);

      expect(decrypted).equal(stringToEncode);
    });
    it('valid data object', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      class TestClass {
        data: string = 'some data';
      }
      const dataToEncode = new TestClass();
      const encrypted = aesToken.encodeAndReturnExpiration(dataToEncode);

      const expirationDate = encrypted.expirationDate;
      const maxExpirationDate = new Date(Date.now() + (aesToken.expiresIn + 1) * 1000);
      const minExpirationDate = new Date(Date.now() + (aesToken.expiresIn - 1) * 1000);

      expect(expirationDate).above(minExpirationDate).below(maxExpirationDate);

      const decrypted = aesToken.decode(encrypted.token);

      expect(decrypted).is.not.string;
      expect(decrypted).is.not.undefined;
      expect((decrypted as TestClass).data).equal(dataToEncode.data);
    });
    it('invalid format', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);

      try {
        aesToken.decode('whatever');
        expect.fail('An error must be thrown');
      } catch (error : any) {
        expect(error).instanceOf(JsonWebTokenError);
      }
    });
    it('expired token', function() {
      const aesToken = new JwtToken(configurationTest.tokenConfiguration.key, configurationTest.tokenConfiguration.expiresIn);
      aesToken.expiresIn = 0;

      const stringToEncode = 'String to encode';
      const encrypted = aesToken.encode(stringToEncode);

      try {
        aesToken.decode(encrypted);
        expect.fail('An error must be thrown');
      } catch (error : any) {
        expect(error).instanceOf(TokenExpiredError);
      }
    });
  });
});
