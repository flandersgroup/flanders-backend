import {LoginControllerInterface} from '@controller/login';
import {ProfileControllerInterface} from '@controller/profile';
import {AuthorizationControllerInterface} from './authorization';

export interface ControllersInterface {
    login : LoginControllerInterface
    profile : ProfileControllerInterface
    authorization : AuthorizationControllerInterface
}
