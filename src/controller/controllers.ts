import {ControllersInterface} from '@controller';
import {RepositoriesInterface} from '@repository';
import {LoginControllerInterface} from '@controller/login';
import {LoginController} from '@controller/login/login';
import {ProfileControllerInterface} from './profile';
import {ProfileController} from './profile/profile';
import {LibrariesInterface} from '@lib';
import {LoginMailerConfiguration} from './login/mailer/configuration';
import {AuthorizationControllerInterface} from './authorization';
import {AuthorizationController} from './authorization/authorization';
export class Controllers implements ControllersInterface {
  constructor(private _loginMailerConfiguration: LoginMailerConfiguration,
    private _libraries : LibrariesInterface,
    private _repositories: RepositoriesInterface) {
    this._libraries.logger.info('Initialisation of the controllers');
  }

  get login() : LoginControllerInterface {
    return new LoginController(this._loginMailerConfiguration, this._libraries, this._repositories);
  }

  get profile() : ProfileControllerInterface {
    return new ProfileController(this._repositories.profile);
  }

  get authorization() : AuthorizationControllerInterface {
    return new AuthorizationController(this._repositories, this._libraries.token);
  }

  get libraries() : LibrariesInterface {
    return this.libraries;
  }

  set libraries(libraries : LibrariesInterface) {
    this._libraries = libraries;
  }

  get repositories() : RepositoriesInterface {
    return this._repositories;
  }

  set repositories(repositories : RepositoriesInterface) {
    this._repositories = repositories;
  }
}
