import {TokenInterface} from '@lib/token';
import {Authorization} from '@model/database/authorization';
import {Profile} from '@model/database/profile';
import {User} from '@model/database/user';
import {RepositoriesInterface} from '@repository';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {AuthorizationControllerInterface} from '.';
import {AuthorizationError} from './error';

export class AuthorizationController implements AuthorizationControllerInterface {
  constructor(private _repositories : RepositoriesInterface,
    private _token: TokenInterface) { }

  loggedInUser(tokenData: string): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      let userEmail: string | object | undefined = undefined;
      try {
        userEmail = this._token.decode(tokenData);
      } catch (error: any) {
        reject(new AuthorizationError(error.message));
      }

      if (userEmail && typeof userEmail === 'string') {
        this._repositories.user.getUserByEmail(userEmail).then((user) => {
          if (user) {
            resolve(user);
          } else {
            reject(new AuthorizationError('Unknown user'));
          }
        }).catch((error : any) => {
          reject(error);
        },
        );
      } else {
        reject(new AuthorizationError('Invalid authorization'));
      }
    });
  }

  getAuthorizationsOfProfile(profileName: string): Promise<AuthorizationResponse[]> {
    return new Promise<AuthorizationResponse[]>((resolve, reject) => {
      this._repositories.profile.getProfileByName(profileName).then((profile) => {
        if (profile && profile.profile_id) {
          this._repositories.authorization.getAuthorizationsOfProfile(profile.profile_id).then((authorizations) => {
            const responseAuthorizations : AuthorizationResponse[] = [];

            for (const authorization of authorizations) {
              const responseAuthorization = new AuthorizationResponse;
              responseAuthorization.name = authorization.authorization_name;
              responseAuthorization.description = authorization.authorization_description;

              responseAuthorizations.push(responseAuthorization);
            }

            resolve(responseAuthorizations);
          }).catch((error) => {
            reject(error);
          });
        } else {
          reject(new AuthorizationError('Unknown profile'));
        }
      }).catch((error) => {
        reject(error);
      });
    });
  }

  getAuthorizationsOfUser(userId: number) : Promise<AuthorizationResponse[]> {
    return new Promise<AuthorizationResponse[]>((resolve, reject) => {
      this._repositories.profile.getProfilesOfUser(userId).then(async (profiles : Profile[]) => {
        const authorizationResponses : AuthorizationResponse[] = [];

        for (const profile of profiles) {
          if (profile.profile_id) {
            const authorizations : Authorization[] = await this._repositories.authorization.getAuthorizationsOfProfile(profile.profile_id);

            for (const authorization of authorizations) {
              const authorizationResponse = new AuthorizationResponse;
              authorizationResponse.name = authorization.authorization_name;
              authorizationResponse.description = authorization.authorization_description;

              authorizationResponses.push(authorizationResponse);
            }
          }
        }

        resolve(authorizationResponses);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  get token(): TokenInterface {
    return this._token;
  }
}
