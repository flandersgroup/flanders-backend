import {User} from '@model/database/user';
import {AuthorizationResponse} from 'common/server/response/authorization';

export interface AuthorizationControllerInterface {
    loggedInUser(tokenData: string) : Promise<User>
    getAuthorizationsOfProfile(profileName: string) : Promise<AuthorizationResponse[]>
    getAuthorizationsOfUser(userId: number) : Promise<AuthorizationResponse[]>
}
