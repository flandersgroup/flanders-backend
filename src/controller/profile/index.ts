import {ProfileResponse} from 'common/server/response/profile';

export interface ProfileControllerInterface {
    getRegistrableProfiles() : Promise<ProfileResponse[]>
    getProfilesOfUser(userId: number) : Promise<ProfileResponse[]>
}
