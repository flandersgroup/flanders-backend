import {ProfileRepositoryInterface} from '@repository/profile';
import {ProfileResponse} from 'common/server/response/profile';
import {ProfileControllerInterface} from '.';

export class ProfileController implements ProfileControllerInterface {
  constructor(private _profileRepository: ProfileRepositoryInterface) {}

  getRegistrableProfiles(): Promise<ProfileResponse[]> {
    return new Promise<ProfileResponse[]>(async (resolve, reject) => {
      this._profileRepository.getRegistrableProfiles().then((profiles) => {
        const profilesResponse : ProfileResponse[] = [];
        for (const profile of profiles) {
          const profileResponse = new ProfileResponse();

          profileResponse.name = profile.profile_name;
          profileResponse.description = profile.profile_description;
          profilesResponse.push(profileResponse);
        }
        resolve(profilesResponse);
      }).catch((error) => reject(error));
    });
  }

  getProfilesOfUser(userId: number) : Promise<ProfileResponse[]> {
    return new Promise<ProfileResponse[]>(async (resolve, reject) => {
      this._profileRepository.getProfilesOfUser(userId).then((profiles) => {
        const profilesResponse : ProfileResponse[] = [];
        for (const profile of profiles) {
          const profileResponse = new ProfileResponse();

          profileResponse.name = profile.profile_name;
          profileResponse.description = profile.profile_description;
          profilesResponse.push(profileResponse);
        }
        resolve(profilesResponse);
      }).catch((error) => reject(error));
    });
  }
}
