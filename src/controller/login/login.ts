import {LoginControllerInterface} from '@controller/login';
import {LoginResponse} from 'common/server/response/login';
import {AuthenticationError, RegisteredError, RegisteredParametersError,
  RegistrationError, RegistrationParametersError, ResetPasswordError,
  ResetPasswordParametersError, ResetPasswordRequestError, ResetPasswordRequestParametersError} from '@controller/login/error';
import {Logger} from '@lib/logger';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {LibrariesInterface} from '@lib';
import {User} from '@model/database/user';
import {LoginMailerConfiguration} from './mailer/configuration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {RepositoriesInterface} from '@repository';
import {LoginMailerFacade} from './mailer/loginMailerFacade';
import {DatabaseError} from '@repository/error';
import {UserProfileRelation} from '@model/database/userProfileRelation';
import {Profile} from '@model/database/profile';

export class LoginController implements LoginControllerInterface {
  private _logger: Logger;
  private _loginMailerFacade : LoginMailerFacade;

  constructor(
      private _loginMailerConfiguration: LoginMailerConfiguration,
      private _libraries: LibrariesInterface,
        private _repositories : RepositoriesInterface) {
    this._logger = this._libraries.logger;
    this._loginMailerFacade = new LoginMailerFacade(this._loginMailerConfiguration, this._libraries);
  }

  login(email: string, password: string): Promise<LoginResponse> {
    return new Promise<LoginResponse>((resolve, reject) => {
      this._repositories.user.getUserByEmail(email).then(async (user) => {
        if (user) {
          if (await this._libraries.hasher.verifyPassword(password, user.user_password)) {
            this._logger.info('User logged in : ' + user.user_email);
            resolve(this._generateLoginResponse(user.user_email));
          } else {
            reject(new AuthenticationError('Wrong password'));
          }
        } else {
          reject(new AuthenticationError('Unknown user'));
        }
      }).catch((error) => reject(error));
    });
  }

  register(registrationParameters: RegistrationParameters): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
      if (registrationParameters.email) {
        this._repositories.user.getUserByEmail(registrationParameters.email).then((user) => {
          if (user) {
            reject(new RegistrationError('User already registered'));
          } else {
            if (registrationParameters.profile !== '') {
              this._repositories.profile.getProfileByName(registrationParameters.profile).then(async (profile) => {
                if (profile && !profile.profile_can_register) {
                  reject(new RegistrationParametersError('The profile choosen is not registrable'));
                } else {
                  registrationParameters.password = await this._libraries.hasher.hashPassword(registrationParameters.password);
                  const mailer = this._loginMailerFacade.loginMailer;
                  mailer.sendRegistrationMail(registrationParameters).then(() => {
                    this._logger.debug('Registration mail successfully send to ' + registrationParameters.email);
                    resolve();
                  }).catch((error) => {
                    reject(error);
                  });
                }
              }).catch((error) => {
                reject(error);
              });
            } else {
              reject(new RegistrationParametersError('Profile id parameter is required'));
            }
          }
        }).catch((error) => {
          reject(error);
        });
      } else {
        reject(new RegistrationParametersError('Email parameter is required'));
      }
    });
  }

  registered(tokenData: string): Promise<LoginResponse> {
    return new Promise<LoginResponse>((resolve, reject) => {
      let decodedData : RegistrationParameters | undefined = undefined;
      try {
        const mailer = this._loginMailerFacade.loginMailer;
        decodedData = mailer.decodeRegistrationParameters(tokenData);
      } catch (error: any) {
        reject(new RegisteredParametersError('The given token data are not valid'));
      }
      if (decodedData && decodedData.email !== '' && decodedData.password !== '' && decodedData.profile !== '') {
        const user = new User;
        user.user_email = decodedData.email;
        user.user_password = decodedData.password;

        this._repositories.profile.getProfileByName(decodedData.profile).then((profile) => {
          if (!profile || !profile.profile_id || !profile.profile_can_register) {
            reject(new RegisteredParametersError('The profile choosen is not registrable'));
          } else {
            if (decodedData) {
              this._registerUser(user, profile).then(() => {
                resolve(this._generateLoginResponse(user.user_email));
              }).catch((error) => {
                reject(error);
              });
            } else {
              reject(new RegisteredParametersError('The given token data are not valid'));
            }
          }
        }).catch((error) => {
          reject(error);
        });
      } else {
        reject(new RegisteredParametersError('The given token data are not valid'));
      }
    });
  }

  private async _registerUser(user: User, profile : Profile) : Promise<void> {
    await this._repositories.databaseConnection.startTransaction();
    try {
      const userId : number = await this._repositories.user.insertUser(user);
      const userProfileRelation = new UserProfileRelation;
      userProfileRelation.user_profile_relation_user_id = userId;
      userProfileRelation.user_profile_relation_profile_id = profile.profile_id as number;
      try {
        await this._repositories.profile.insertUserProfileRelation(userProfileRelation);
        this._repositories.databaseConnection.commitTransaction();
      } catch (error) {
        if (error instanceof DatabaseError) {
          throw new RegisteredError('Cannot add the given profile to the user');
        } else {
          throw error;
        }
      }
    } catch (error) {
      this._repositories.databaseConnection.rollbackTransaction();
      if (error instanceof DatabaseError) {
        throw new RegisteredError('User already registered');
      } else {
        throw error;
      }
    }
  }

  resetPasswordRequest(resetPasswordParameters: ResetPasswordRequestParameters): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (resetPasswordParameters.email) {
        this._repositories.user.getUserByEmail(resetPasswordParameters.email).then((user) => {
          if (user) {
            const mailer = this._loginMailerFacade.loginMailer;
            mailer.sendResetPasswordMail(resetPasswordParameters).then(() => {
              this._logger.debug('Reset password mail successfully send to ' + resetPasswordParameters.email);
              resolve();
            }).catch((error) => {
              reject(error);
            });
          } else {
            reject(new ResetPasswordRequestError('Cannot find the given user email'));
          }
        }).catch((error) => {
          reject(error);
        });
      } else {
        reject(new ResetPasswordRequestParametersError('Email parameter is required'));
      }
    });
  }

  resetPassword(tokenData: string, password: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      let decodedData;
      try {
        const mailer = this._loginMailerFacade.loginMailer;
        decodedData = mailer.decodeResetPasswordParameters(tokenData);
      } catch (error: any) {
        reject(new ResetPasswordParametersError('The given token data are not valid'));
      }
      if (decodedData && decodedData.email !== '') {
        this._repositories.user.getUserByEmail(decodedData.email).then(async (user) => {
          if (user && user.user_id) {
            const hashedPassword = await this._libraries.hasher.hashPassword(password);
            this._repositories.user.updatePassword(user.user_id, hashedPassword).then(() => {
              resolve();
            }).catch((error) => {
              reject(error);
            });
          } else {
            reject(new ResetPasswordError('Cannot find the given user email'));
          }
        }).catch((error) => {
          reject(error);
        });
      } else {
        reject(new ResetPasswordParametersError('The given token data are not valid'));
      }
    });
  }

  _generateLoginResponse(email: string): LoginResponse {
    const tokenReponse = this._libraries.token.encodeAndReturnExpiration(email);
    const loginResponse = new LoginResponse();
    loginResponse.expires = tokenReponse.expirationDate;
    loginResponse.token = tokenReponse.token;
    return loginResponse;
  }

  get loginMailerFacade() : LoginMailerFacade {
    return this._loginMailerFacade;
  }
  set loginMailerFacade(loginMailerFacade : LoginMailerFacade) {
    this._loginMailerFacade = loginMailerFacade;
  }
}
