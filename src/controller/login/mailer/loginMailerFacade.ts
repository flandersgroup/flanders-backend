import {LibrariesInterface} from '@lib';
import {LoginMailerInterface} from '.';
import {LoginMailerConfiguration} from './configuration';
import {LoginMailer} from './mailer';

export class LoginMailerFacade {
  constructor(
        private _loginMailerConfiguration: LoginMailerConfiguration,
        private _libraries: LibrariesInterface,
  ) {}
  get loginMailer() : LoginMailerInterface {
    return new LoginMailer(this._loginMailerConfiguration, this._libraries.mailer, this._libraries.mailerToken);
  }
}
