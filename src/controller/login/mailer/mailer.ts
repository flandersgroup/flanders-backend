import {MailerInterface} from '@lib/mailer';
import {TokenInterface} from '@lib/token';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {LoginMailerInterface} from '.';
import {LoginMailerConfiguration} from './configuration';

export class LoginMailer implements LoginMailerInterface {
  constructor(private _loginMailerConfiguration: LoginMailerConfiguration,
    private _mailer : MailerInterface, private _token: TokenInterface) {}

  sendRegistrationMail(registrationParameters: RegistrationParameters) : Promise<any> {
    this._mailer.to = registrationParameters.email;
    this._mailer.subject = 'Welcome to the flanders company, validate your registration';

    const encodedParameters = this.encodeRegistrationParameters(registrationParameters);
    const registrationLink = this._loginMailerConfiguration.frontendRegistratedApi + encodedParameters;

    this._mailer.html = '<h1>Welcome to the flanders company</h1>' +
    `<div>Click on this <a href="${registrationLink}">link to validate your registration</a></div>` +
     '<div>This link will expire in 24 hours</div>';

    return this._mailer.sendMail();
  }

  decodeRegistrationParameters(encodedData : string) : RegistrationParameters {
    return this._token.decode(encodedData) as RegistrationParameters;
  }

  encodeRegistrationParameters(registrationParameters: RegistrationParameters): string {
    return this._token.encode(registrationParameters);
  }

  get token() : TokenInterface {
    return this._token;
  }

  get mailer() : MailerInterface {
    return this._mailer;
  }

  set mailer(mailer : MailerInterface) {
    this._mailer = mailer;
  }

  sendResetPasswordMail(resetPasswordParameters : ResetPasswordRequestParameters) : Promise<any> {
    this._mailer.to = resetPasswordParameters.email;
    this._mailer.subject = 'Flanders company, your password reset request';

    const encodedParameters = this.encodeResetPasswordParameters(resetPasswordParameters);
    const registrationLink = this._loginMailerConfiguration.frontendResetPasswordApi + encodedParameters;

    this._mailer.html = '<h1>You send a request to reset your password.</h1>' +
    `<div>Click on this <a href="${registrationLink}">link to reset your password</a></div>` +
    '<div>This link will expire in 24 hours</div>';

    return this._mailer.sendMail();
  }

  decodeResetPasswordParameters(encodedData : string) : ResetPasswordRequestParameters {
    return this._token.decode(encodedData) as ResetPasswordRequestParameters;
  }

  encodeResetPasswordParameters(resetPasswordParameters: ResetPasswordRequestParameters) : string {
    return this._token.encode(resetPasswordParameters);
  }
}
