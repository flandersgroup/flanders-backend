export interface LoginMailerConfiguration {
    frontendRegistratedApi : string
    frontendResetPasswordApi : string
    encodingKey : string
    registrationLinkExpiresIn : number
}
