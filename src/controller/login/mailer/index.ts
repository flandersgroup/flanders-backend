import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';

export interface LoginMailerInterface {
    sendRegistrationMail(registrationParameters: RegistrationParameters) : Promise<any>
    decodeRegistrationParameters(encodedData : string) : RegistrationParameters
    encodeRegistrationParameters(registrationParameters: RegistrationParameters) : string

    sendResetPasswordMail(resetPasswordParameters : ResetPasswordRequestParameters) : Promise<any>
    decodeResetPasswordParameters(encodedData : string) : ResetPasswordRequestParameters
    encodeResetPasswordParameters(resetPasswordParameters: ResetPasswordRequestParameters) : string
}
