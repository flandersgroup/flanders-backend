export class AuthenticationError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class RegistrationError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class RegistrationParametersError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class RegisteredError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class RegisteredParametersError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class ResetPasswordRequestError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class ResetPasswordRequestParametersError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class ResetPasswordError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class ResetPasswordParametersError extends Error {
  constructor(message?: string) {
    super(message);
  }
}
