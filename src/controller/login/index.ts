import {LoginResponse} from 'common/server/response/login';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';

export interface LoginControllerInterface {
    login(email: string, password: string): Promise<LoginResponse>
    register(registrationParameters: RegistrationParameters): Promise<void>
    registered(tokenData : string) : Promise<LoginResponse>
    resetPasswordRequest(resetPasswordParameters : ResetPasswordRequestParameters) : Promise<void>
    resetPassword(tokenData : string, password : string) : Promise<void>
}
