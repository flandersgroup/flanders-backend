import {Logger} from '@lib/logger';
import {HasherInterface} from '@lib/hasher';
import {TokenInterface} from '@lib/token';
import {MailerInterface} from './mailer';

export interface LibrariesInterface {
    logger: Logger
    hasher: HasherInterface
    token: TokenInterface
    mailer: MailerInterface
    mailerToken: TokenInterface
}
