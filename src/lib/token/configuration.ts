export interface TokenConfiguration {
    key: string
    expiresIn: number
}
