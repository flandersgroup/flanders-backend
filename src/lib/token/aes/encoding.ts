import crypto from 'crypto';
import {DecodingError} from '@lib/token/aes/error';

export class AesEncoding {
  constructor(public key : string) {}

  encode(payload: string): string {
    // Defining initialization vector
    const iv = crypto.randomBytes(16);

    const cipher = crypto.createCipheriv(
        'aes-256-cbc', this.key, iv);

    let encrypted = cipher.update(payload, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return encrypted + '.' + iv.toString('hex');
  }

  decode(payload: string): string {
    const splited = payload.split('.');
    if (splited.length !== 2) {
      throw new DecodingError('The string to decode is not in the right format');
    }

    const encrypted = Buffer.from(splited[0], 'hex');
    const iv = Buffer.from(splited[1], 'hex');

    try {
      const cipher = crypto.createDecipheriv(
          'aes-256-cbc', this.key, iv);

      const decrypted = Buffer.concat([cipher.update(encrypted), cipher.final()]);
      return decrypted.toString();
    } catch (error : any) {
      throw new DecodingError(error.message);
    }
  }
}
