import {TokenInterface} from '@lib/token';
import {TokenData} from '@lib/token/aes/data';
import {AesEncoding} from '@lib/token/aes/encoding';
import {DecodingError, TokenExpired} from '@lib/token/aes/error';
import {TokenResponse} from '@lib/token/data';

export class AesToken implements TokenInterface {
  private _aesEncoding = new AesEncoding(this.key);

  constructor(public key: string, public expiresIn: number) { }

  encode(payload: object | string): string {
    return this.encodeAndReturnExpiration(payload).token;
  }

  encodeAndReturnExpiration(payload: object | string): TokenResponse {
    const tokenData = new TokenData();
    tokenData.expirationDate = new Date(Date.now() + this.expiresIn * 1000);
    tokenData.data = JSON.stringify(payload);

    const encoded = this._aesEncoding.encode(JSON.stringify(tokenData));

    const tokenResponse = new TokenResponse();
    tokenResponse.expirationDate = tokenData.expirationDate;
    tokenResponse.token = encoded;

    return tokenResponse;
  }

  decode(payload: string): object | string {
    const decodedData = this._aesEncoding.decode(payload);

    let decoded: TokenData = new TokenData;

    try {
      decoded = JSON.parse(decodedData);
    } catch (error: any) {
      throw new DecodingError('Unable to decode given data');
    }

    if (new Date(decoded.expirationDate) > new Date()) {
      return JSON.parse(decoded.data);
    } else {
      throw new TokenExpired('Token expired');
    }
  }
}
