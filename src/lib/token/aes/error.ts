export class DecodingError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class TokenExpired extends DecodingError {
  constructor(message?: string) {
    super(message);
  }
}
