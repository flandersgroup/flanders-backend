import {TokenInterface} from '@lib/token';
import jsonwebtoken from 'jsonwebtoken';
import {TokenResponse} from '@lib/token/data';
import {TokenData} from '@lib/token/jwt/data';

export class JwtToken implements TokenInterface {
  constructor(public key : string, public expiresIn : number) {}

  encode(payload: object | string): string {
    const tokenData = new TokenData();
    tokenData.data = payload;
    return jsonwebtoken.sign({tokenData}, this.key, {expiresIn: this.expiresIn});
  }

  encodeAndReturnExpiration(payload: object | string) : TokenResponse {
    const encoded = this.encode(payload);

    const completeJson : any = jsonwebtoken.decode(encoded, {complete: true});
    const expirationDate = new Date(0);
    expirationDate.setUTCSeconds(completeJson.payload.exp);

    const tokenResponse = new TokenResponse();
    tokenResponse.expirationDate = expirationDate;
    tokenResponse.token = encoded;

    return tokenResponse;
  }

  decode(payload: string): object | string {
    const returnData = jsonwebtoken.verify(payload, this.key);
    return (returnData as any).tokenData.data;
  }
}
