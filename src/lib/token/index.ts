import {TokenResponse} from './data';

export interface TokenInterface {
    expiresIn : number
    key : string

    encode(payload: object | string) : string
    encodeAndReturnExpiration(payload: object | string) : TokenResponse
    decode(payload: string) : object | string
}
