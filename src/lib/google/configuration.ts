export class GoogleAuthenticationConfiguration {
  clientId: string | undefined = undefined;
  clientSecret: string | undefined = undefined;
  refreshToken: string | undefined = undefined;
  accessToken: string | undefined = undefined;
  scope?: string;
}
