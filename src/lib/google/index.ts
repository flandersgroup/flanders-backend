import {OAuth2Client} from 'google-auth-library/build/src/auth/oauth2client';
import {GoogleAuthenticationConfiguration} from './configuration';

export class GoogleAuthentication {
  constructor(private _configuration: GoogleAuthenticationConfiguration) {
  }

  getAuthentication() : OAuth2Client {
    const authClient: OAuth2Client = new OAuth2Client({
      clientId: this._configuration.clientId,
      clientSecret: this._configuration.clientSecret,
    });

    authClient.setCredentials({access_token: this._configuration.accessToken, refresh_token: this._configuration.refreshToken, scope: 'https://mail.google.com/'});

    return authClient;
  }
}
