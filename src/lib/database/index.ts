import {DatabaseConfiguration} from '@lib/database/configuration';
import knex, {Knex} from 'knex';

export class Database {
  private _connection: Knex | undefined;

  constructor(private _configuration: DatabaseConfiguration) {
  }

  async disconnect() {
    if (this._connection) {
      await this._connection.destroy();
      this._connection = undefined;
    }
  }

  getConnection(): Knex {
    if (!this._connection) {
      const config: Knex.Config = {
        client: 'pg',
        connection: {
          host: this._configuration.host,
          port: this._configuration.port,
          user: this._configuration.user,
          password: this._configuration.password,
          database: this._configuration.database,
          connectionString: this._configuration.connectionString,
        },
      };
      this._connection = knex(config);
    }

    return this._connection;
  }
}
