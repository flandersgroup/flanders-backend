export interface DatabaseConfiguration {
    host?: string
    port?: number
    user?: string
    password?: string
    database?: string
    connectionString?: string
}
