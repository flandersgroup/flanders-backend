import {Configuration} from '@configuration/config.global';
import winston from 'winston';
import {Logger} from '@lib/logger';

export class WinstonLogger {
  static createLogger(configuration: Configuration): Logger {
    const customFormat = winston.format.combine(
        winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
        winston.format.printf(({level, message, timestamp}) => {
          return `${level}: ${message} (${timestamp as string})`;
        }),
    );

    const loggingLevel = configuration.logLevel ?? 'debug';

    const options: winston.LoggerOptions = {
      transports: [
        new winston.transports.Console({
          level: loggingLevel,
          format: winston.format.combine(winston.format.colorize(), customFormat),
        }),
        // Example of logging to a  log file
        // new winston.transports.File({ filename: "debug.log", level: "debug", format: customFormat })
      ],
    };

    const logger = winston.createLogger(options);

    logger.debug('Logging initialized at level' + loggingLevel);

    return logger;
  }
}
