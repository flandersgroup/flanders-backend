export interface Logger {
    error(message: any): Logger
    warn(message: any): Logger
    info(message: any): Logger
    debug(message: any): Logger
    silly(message: any): Logger

    close(): Logger
}
