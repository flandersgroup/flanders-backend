import {LibrariesInterface} from '@lib';
import {Logger} from '@lib/logger';
import {HasherInterface} from '@lib/hasher';
import {TokenInterface} from '@lib/token';
import {Configuration} from '@configuration/config.global';
import {WinstonLogger} from '@lib/logger/winston';
import {BCryptHasher} from '@lib/hasher/hasher';
import {AesToken} from '@lib/token/aes';
import {MailerInterface} from './mailer';
import {GoogleMailer} from './mailer/gmail';

export class Libraries implements LibrariesInterface {
  private _logger: Logger;

  constructor(private _configuration: Configuration) {
    this._logger = WinstonLogger.createLogger(_configuration);
  }

  get logger() : Logger {
    return this._logger;
  }

  get hasher(): HasherInterface {
    return new BCryptHasher();
  }

  get token() : TokenInterface {
    return new AesToken(this._configuration.tokenConfiguration.key, this._configuration.tokenConfiguration.expiresIn); ;
  }

  get mailer() : MailerInterface {
    return new GoogleMailer(this._configuration.mailerConfiguration); ;
  }

  get mailerToken() : TokenInterface {
    return new AesToken(this._configuration.loginMailerConfiguration.encodingKey,
        this._configuration.loginMailerConfiguration.registrationLinkExpiresIn);
  }
}
