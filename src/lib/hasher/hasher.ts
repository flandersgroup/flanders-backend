import * as bcrypt from 'bcryptjs';
import {HasherInterface} from '@lib/hasher';

export class BCryptHasher implements HasherInterface {
  public async hashPassword(password: string): Promise<string> {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hash(password, salt);
  }

  public verifyPassword(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }
}
