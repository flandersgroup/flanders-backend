import {MailerInterface} from '.';
// eslint-disable-next-line camelcase
import {gmail_v1} from 'googleapis/build/src/apis/gmail/v1';
import {GoogleAuthenticationConfiguration} from '@lib/google/configuration';
import {GoogleAuthentication} from '@lib/google';

export class GoogleMailer implements MailerInterface {
  from: string = '';
  to: string = '';
  subject: string = '';
  html: string = '';

  constructor(private _googleAuthenticationConfiguration: GoogleAuthenticationConfiguration) {
  }

  sendMail(): Promise<any> {
    const authClient = new GoogleAuthentication(this._googleAuthenticationConfiguration).getAuthentication();

    const messageParts = [
      `From: ${this.from}`,
      `To: ${this.to}`,
      'Content-Type: text/html; charset=utf-8',
      'MIME-Version: 1.0',
      `Subject: ${this.subject}`,
      '',
      this.html,
    ];
    const message = messageParts.join('\n');

    // The body needs to be base64url encoded.
    const encodedMessage = Buffer.from(message)
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');

    // eslint-disable-next-line camelcase
    const gmail = new gmail_v1.Gmail({
      auth: authClient,
    });
    const res = gmail.users.messages.send({
      requestBody: {
        raw: encodedMessage,
      },
      userId: 'me',
    });

    return res;
  }
}
