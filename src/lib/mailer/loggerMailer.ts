import {Logger} from '@lib/logger';
import {MailerInterface} from '.';

export class LoggerMailer implements MailerInterface {
  from : string= '';
  to : string= '';
  subject : string= '';
  text : string= '';
  html : string= '';

  constructor(private _logger: Logger) {}

  sendMail() : Promise<any> {
    this._logger.info('Received mail to send');
    this._logger.info('From : ' + this.from);
    this._logger.info('To : ' + this.to);
    this._logger.info('Subject : ' + this.subject);
    this._logger.info('Text : ' + this.text);
    this._logger.info('Html : ' + this.html);
    return new Promise<void>((resolve) => {
      resolve();
    });
  }
}
