export interface MailerInterface {
    from : string
    to : string
    subject : string
    html : string

    sendMail() : Promise<any>
}
