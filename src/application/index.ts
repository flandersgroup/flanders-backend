import express from 'express';
import {Server} from 'http';
import {Controllers} from '@controller/controllers';
import {ServerInitialization} from '@server';
import {ConfigurationLoader} from '@configuration';
import {Configuration} from '@configuration/config.global';
import {Repositories} from '@repository/repositories';
import cors from 'cors';
import {RepositoriesInterface} from '@repository';
import {ControllersInterface} from '@controller';
import {LibrariesInterface} from '@lib';
import {Libraries} from '@lib/libraries';

export class Application {
  private _application = express();
  private _server: Server | undefined = undefined;
  private _libraries: LibrariesInterface | undefined = undefined;
  private _repositories: RepositoriesInterface | undefined = undefined;
  private _controllers: ControllersInterface | undefined = undefined;
  private _serverInitialization: ServerInitialization | undefined = undefined;

  constructor() { }

  setup(environnement: string | undefined) {
    process.on('SIGINT', async () => {
      await this.shutdownApplication();
    } );
    process.on('SIGQUIT', async () => {
      await this.shutdownApplication();
    });
    process.on('SIGTERM', async () => {
      await this.shutdownApplication();
    });

    const configuration = ConfigurationLoader.configuration(environnement);
    const port = configuration.port;

    console.log('Configuration loaded for env : ' + environnement);

    this.prepare();
    this.initialize(configuration);

    this._server = this._application.listen(port, () => {
      this._libraries?.logger.info('Server connected on port ' + port.toString());
    });
  }

  private prepare(): void {
    this._application.use(express.json());
    this._application.use(cors());
  }

  private initialize(configuration: Configuration) {
    this._libraries = new Libraries(configuration);
    this._repositories = new Repositories(this._libraries.logger, configuration.databaseConfiguration);
    this._controllers = new Controllers(configuration.loginMailerConfiguration, this._libraries, this._repositories);

    this._serverInitialization = new ServerInitialization(this._libraries.logger, this._controllers);
    this._serverInitialization.initialize(this._application);
  }

  get application(): Express.Application {
    return this._application;
  }

  async shutdownApplication() {
    this._libraries?.logger.info('Shutdown of application');
    this._server?.close();
    this._libraries?.logger.close();
    process.exitCode = 0;
  }

  get repositories(): RepositoriesInterface | undefined {
    return this._repositories;
  }

  set repositories(repositories : RepositoriesInterface | undefined) {
    this._repositories = repositories;
    if (this._controllers && repositories) {
      (this._controllers as Controllers).repositories = repositories;
    }
  }

  get libraries(): LibrariesInterface | undefined {
    return this._libraries;
  }

  set libraries(libraries : LibrariesInterface | undefined) {
    this._libraries = libraries;
    if (this._controllers && libraries) {
      (this._controllers as Controllers).libraries = libraries;
    }
  }

  get controllers(): ControllersInterface | undefined {
    return this._controllers;
  }

  set controllers(controllers : ControllersInterface | undefined) {
    this._controllers = controllers;
    if (this._serverInitialization && controllers) {
      this._serverInitialization.controllers = controllers;
    }
  }

  get server() : ServerInitialization | undefined {
    return this._serverInitialization;
  }
}
