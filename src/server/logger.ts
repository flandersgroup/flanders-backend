import express from 'express';
import {Logger} from '@lib/logger';
import {InitializationInterface} from '@server/interface';

export class LoggerInitialization implements InitializationInterface {
  constructor(private _logger: Logger) {}

  initialize(application: express.Application): void {
    application.get('/*', (request, response, next) => {
      this._logger.debug('Receive get request ' + request.url + ', headers : ' +
       JSON.stringify(request.headers) + ', body :' + this.bodyToString(request.body));
      next();
    });

    application.post('/*', (request, response, next) => {
      this._logger.debug('Receive post request ' + request.url + ', headers : ' +
      JSON.stringify(request.headers) + ', body :' + this.bodyToString(request.body));
      next();
    });

    application.put('/*', (request, response, next) => {
      this._logger.debug('Receive put request ' + request.url + ', headers : ' + JSON.stringify(request.headers) +
       ', body :' + this.bodyToString(request.body));
      next();
    });

    application.delete('/*', (request, response, next) => {
      this._logger.debug('Receive put request ' + request.url + ', headers : ' + JSON.stringify(request.headers) +
      ', body :' + this.bodyToString(request.body));
      next();
    });
  }

  bodyToString(body : any) {
    if (body && body.password) {
      const opfuscatedBody = JSON.parse(JSON.stringify(body));
      opfuscatedBody.password = '*****';
      return JSON.stringify(opfuscatedBody);
    } else {
      return JSON.stringify(body);
    }
  }
}
