import express from 'express';

export interface InitializationInterface {
    initialize(application: express.Application) : void
}
