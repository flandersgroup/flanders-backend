import {ControllersInterface} from '@controller';
import {AuthorizationError} from '@controller/authorization/error';
import {Logger} from '@lib/logger';
import {User} from '@model/database/user';
import {GenericRequestResponseErrorMessage} from '@server/messages';
import {Request, Response, NextFunction} from 'express';

export class Authorization {
  constructor(private _logger: Logger, private _controllers: ControllersInterface) { }

  userAuthorization(request: Request, response: Response, next: NextFunction): void {
    const authorisationHeader = request.headers.authorization;
    if (authorisationHeader && authorisationHeader.startsWith('Bearer ')) {
      const token = authorisationHeader.substring(7, authorisationHeader.length);
      this._controllers.authorization.loggedInUser(token).then((user: User) => {
        response.locals.user = user;
        next();
      }).catch((error) => {
        if (error instanceof AuthorizationError) {
          response.status(401);
        } else {
          response.status(500);
        }
        response.send(error.message);
      });
    } else {
      response.status(401).send('Unauthorized');
    }
  }

  isLoggedIn(request: Request, response: Response): void {
    response.status(200).send();
  }

  getAuthorization(request: Request, response: Response): void {
    const user: User = response.locals.user;
    if (user && user.user_id) {
      this._controllers.authorization.getAuthorizationsOfUser(user.user_id).then((authorizations) => {
        response.send(authorizations);
      })
          .catch((error) => {
            this._logger.error(error.message);
            response.status(500).send(error.message);
          });
    } else {
      this._logger.error('Unexpected null user');
      response.status(500).send('Unexpected error');
    }
  }

  getAuthorizationsOfProfile(request: Request, response: Response): void {
    const user: User = response.locals.user;
    const profileName: string | undefined = request.query.profileName as string | undefined;

    if (!profileName || profileName === '') {
      response.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    } else {
      if (user && user.user_id) {
        this._controllers.profile.getProfilesOfUser(user.user_id).then((profiles) => {
          let found = false;
          for (const profile of profiles) {
            if (profile.name == profileName) {
              found = true;
            }
          }

          if (found == false) {
            response.status(401).send('Unauthorized');
          } else {
            this._controllers.authorization.getAuthorizationsOfProfile(profileName).then((authorizations) => {
              response.status(200).send(authorizations);
            }).catch((error) => {
              if (error instanceof AuthorizationError) {
                response.status(401);
              } else {
                response.status(500);
              }
              response.send(error.message);
            });
          }
        })
            .catch((error) => {
              this._logger.error(error.message);
              response.status(500).send(error.message);
            });
      } else {
        this._logger.error('Unexpected null user');
        response.status(500).send('Unexpected error');
      }
    }
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }
  set controllers(controllers: ControllersInterface) {
    this._controllers = controllers;
  }
}
