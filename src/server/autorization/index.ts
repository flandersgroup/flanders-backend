import {ControllersInterface} from '@controller';
import {Logger} from '@lib/logger';
import {InitializationInterface} from '@server/interface';
import express from 'express';
import {Authorization} from './autorization';

export class AutorizationInitialization implements InitializationInterface {
  constructor(private _logger: Logger, private _controllers : ControllersInterface) {
  }

  initialize(application: express.Application): void {
    application.get('/isLoggedIn',
        (request, response, next) => {
          this.authorization.userAuthorization(request, response, next);
        },
        (request, response) => {
          this.authorization.isLoggedIn(request, response);
        });
    application.get('/authorization',
        (request, response, next) => {
          this.authorization.userAuthorization(request, response, next);
        },
        (request, response) => {
          this.authorization.getAuthorization(request, response);
        });
    application.get('/authorizationsOfProfile',
        (request, response, next) => {
          this.authorization.userAuthorization(request, response, next);
        },
        (request, response) => {
          this.authorization.getAuthorizationsOfProfile(request, response);
        });
  }

  get authorization() : Authorization {
    return new Authorization(this._logger, this._controllers);
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }

  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
  }
}
