import {Request, Response} from 'express';
import {Logger} from '@lib/logger';
import {ControllersInterface} from '@controller';
import {User} from '@model/database/user';

export class Profile {
  constructor(private _logger: Logger, private _controllers : ControllersInterface) { }

  registrableProfiles(request: Request, response: Response): void {
    this._controllers.profile.getRegistrableProfiles().then((result) => {
      response.send(result);
    }).catch((error) => {
      response.status(500);

      this._logger.error('An error occured during profile request ' + error.message);
      response.send(error.message);
    });
  }

  profilesOfUser(request: Request, response: Response): void {
    const user: User = response.locals.user;
    if (user && user.user_id) {
      this._controllers.profile.getProfilesOfUser(user.user_id).then((profiles) => {
        response.send(profiles);
      })
          .catch((error) => {
            this._logger.error(error.message);
            response.status(500).send(error.message);
          });
    } else {
      this._logger.error('Unexpected null user');
      response.status(500).send('Unexpected error');
    }
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }
  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
  }
}
