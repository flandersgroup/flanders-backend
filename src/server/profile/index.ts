import {ControllersInterface} from '@controller';
import {Logger} from '@lib/logger';
import {Authorization} from '@server/autorization/autorization';
import {InitializationInterface} from '@server/interface';
import express from 'express';
import {Profile} from './profile';

export class ProfileInitialization implements InitializationInterface {
  constructor(private _logger: Logger, private _controllers : ControllersInterface) {
  }

  initialize(application: express.Application): void {
    application.get('/registrableProfiles', (request, response) => {
      this.profile.registrableProfiles(request, response);
    });
    application.get('/profilesOfUser', (request, response, next) => {
      this.authorization.userAuthorization(request, response, next);
    },
    (request, response) => {
      this.profile.profilesOfUser(request, response);
    });
  }

  get authorization() : Authorization {
    return new Authorization(this._logger, this._controllers);
  }

  get profile() : Profile {
    return new Profile(this._logger, this._controllers);
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }
  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
  }
}
