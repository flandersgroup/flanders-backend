import express from 'express';
import {Logger} from '@lib/logger';
import {ControllersInterface} from '@controller';
import {LoginInitialization as LoginInitialization} from '@server/login';
import {StatusInitialization} from '@server/status';
import {ProfileInitialization} from '@server/profile';
import {LoggerInitialization} from './logger';
import {AutorizationInitialization} from './autorization';

export class ServerInitialization {
  private _loginInitialization: LoginInitialization;
  private _profileInitialization : ProfileInitialization;
  private _authorizationInitialization : AutorizationInitialization;

  constructor(private _logger: Logger, private _controllers: ControllersInterface) {
    this._loginInitialization = new LoginInitialization(this._logger, this._controllers);
    this._profileInitialization = new ProfileInitialization(this._logger, this._controllers);
    this._authorizationInitialization = new AutorizationInitialization(this._logger, this._controllers);
  }

  initialize(application: express.Application): void {
    new LoggerInitialization(this._logger).initialize(application);
    new StatusInitialization().initialize(application);

    this._loginInitialization.initialize(application);
    this._profileInitialization.initialize(application);
    this._authorizationInitialization.initialize(application);
  }

  get loginInitialization(): LoginInitialization {
    return this._loginInitialization;
  }

  get profileInitialization() : ProfileInitialization {
    return this._profileInitialization;
  }

  get authorizationInitialization() : AutorizationInitialization {
    return this._authorizationInitialization;
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }

  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
    this._loginInitialization.controllers = controllers;
    this._profileInitialization.controllers = controllers;
    this._authorizationInitialization.controllers = controllers;
  }
}
