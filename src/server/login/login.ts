import {Request, Response} from 'express';
import {Logger} from '@lib/logger';
import {AuthenticationParameters} from 'common/server/parameter/authentication';
import {AuthenticationError, RegisteredError, RegisteredParametersError,
  RegistrationError, RegistrationParametersError, ResetPasswordError,
  ResetPasswordParametersError, ResetPasswordRequestError,
  ResetPasswordRequestParametersError} from '@controller/login/error';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {ResetPasswordRequestParameters}
  from 'common/server/parameter/resetPasswordRequest';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {GenericRequestResponseErrorMessage} from '@server/messages';
import {ControllersInterface} from '@controller';

export class Login {
  constructor(private _logger: Logger, private _controllers : ControllersInterface) { }

  login(req: Request, res: Response): void {
    const authentication = req.body as AuthenticationParameters;
    if (authentication && authentication.email != null && authentication.password != null) {
      this._controllers.login.login(authentication.email, authentication.password).then((result) => {
        res.send(result);
      }).catch((error) => {
        if (error instanceof AuthenticationError) {
          res.status(401);
        } else {
          res.status(500);
        }

        this._logger.error('An error occured during login : ' + error.message);
        res.send(error.message);
      });
    } else {
      res.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    }
  }

  register(req: Request, res: Response): void {
    const registrationParameters = req.body as RegistrationParameters;

    if (registrationParameters && registrationParameters.email != null &&
        registrationParameters.password != null && registrationParameters.profile != null) {
      this._controllers.login.register(registrationParameters).then(() => {
        res.status(204).send();
      }).catch((error) => {
        if (error instanceof RegistrationError) {
          res.status(409);
        } else if (error instanceof RegistrationParametersError) {
          res.status(400);
        } else {
          res.status(500);
        }
        this._logger.error('An error occured during registration : ' + error.message);
        res.send(error.message);
      });
    } else {
      res.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    }
  }

  registered(req: Request, res: Response): void {
    const registeredParameters = req.body as RegisteredParameters;

    if (registeredParameters && registeredParameters.tokenData) {
      this._controllers.login.registered(registeredParameters.tokenData).then((result) => {
        res.send(result);
      }).catch((error) => {
        if (error instanceof RegisteredError) {
          res.status(409);
        } else if (error instanceof RegisteredParametersError) {
          res.status(400);
        } else {
          res.status(500);
        }

        this._logger.error('An error occured while registered : ' + error.message);
        res.send(error.message);
      });
    } else {
      res.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    }
  }

  resetPasswordRequest(req: Request, res: Response): void {
    const resetPasswordParameters = req.body as ResetPasswordRequestParameters;

    if (resetPasswordParameters && resetPasswordParameters.email) {
      this._controllers.login.resetPasswordRequest(resetPasswordParameters).then(() => {
        res.status(204).send();
      }).catch((error) => {
        if (error instanceof ResetPasswordRequestError) {
          res.status(404);
        } else if (error instanceof ResetPasswordRequestParametersError) {
          res.status(400);
        } else {
          res.status(500);
        }

        this._logger.error('An error occured during reset password request : ' + error.message);
        res.send(error.message);
      });
    } else {
      res.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    }
  }

  resetPassword(req: Request, res: Response): void {
    const resetPasswordParameters = req.body as ResetPasswordParameters;

    if (resetPasswordParameters && resetPasswordParameters.tokenData && resetPasswordParameters.password) {
      this._controllers.login.resetPassword(resetPasswordParameters.tokenData, resetPasswordParameters.password).then(() => {
        res.status(204).send();
      }).catch((error) => {
        if (error instanceof ResetPasswordError) {
          res.status(404);
        } else if (error instanceof ResetPasswordParametersError) {
          res.status(400);
        } else {
          res.status(500);
        }

        this._logger.error('An error occured during reset password request : ' + error.message);
        res.send(error.message);
      });
    } else {
      res.status(400).send(GenericRequestResponseErrorMessage.invalidRequestParameters);
    }
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }
  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
  }
}
