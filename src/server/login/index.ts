import express from 'express';
import {Logger} from '@lib/logger';
import {Login} from '@server/login/login';
import {InitializationInterface} from '@server/interface';
import {ControllersInterface} from '@controller';

export class LoginInitialization implements InitializationInterface {
  constructor(private _logger: Logger, private _controllers : ControllersInterface) {
  }

  initialize(application: express.Application): void {
    application.post('/login', (request, response) => {
      this.authentication.login(request, response);
    });
    application.post('/register', (request, response) => {
      this.authentication.register(request, response);
    });
    application.post('/registered', (request, response) => {
      this.authentication.registered(request, response);
    });
    application.post('/resetPasswordRequest', (request, response) => {
      this.authentication.resetPasswordRequest(request, response);
    });
    application.post('/resetPassword', (request, response) => {
      this.authentication.resetPassword(request, response);
    });
  }

  get authentication(): Login {
    return new Login(this._logger, this._controllers);
  }

  get controllers(): ControllersInterface {
    return this._controllers;
  }
  set controllers(controllers : ControllersInterface) {
    this._controllers = controllers;
  }
}
