import express from 'express';
import {InitializationInterface} from './interface';

export class StatusInitialization implements InitializationInterface {
  initialize(application: express.Application) : void {
    application.get('/status', (request, response) => {
      response.status(200).send({
        status: 'Ok',
        environnement: process.env.NODE_ENV,
      });
    });
  }
}
