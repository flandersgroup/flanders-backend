export class GenericRequestResponseErrorMessage {
  static get invalidRequestParameters() : string {
    return 'Invalid request parameters';
  }
}
