import {User} from '@model/database/user';

export interface UserRepositoryInterface {
    getUserByEmail(email: string): Promise<User | undefined>
    insertUser(user : User) : Promise<number>
    updatePassword(userId: number, password: string) : Promise<void>
}
