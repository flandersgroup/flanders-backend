import {UserRepositoryInterface} from '@repository/user';
import {User, UserColumns} from '@model/database/user';
import {DatabaseError} from '@repository/error';
import {DatabaseConnectionInterface} from '@repository/base';

export class UserRepository implements UserRepositoryInterface {
  private _userTableName : string = 'flanders_user';

  constructor(private _databaseConnection : DatabaseConnectionInterface) {
  }

  getUserByEmail(email: string): Promise<User | undefined> {
    return new Promise<User | undefined>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._userTableName).where(UserColumns.user_email_column, email).first();
      promiseRow.then((row) => {
        resolve(row);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  insertUser(user: User): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseResult = connection.insert(user).into(this._userTableName).returning(UserColumns.user_id_column);
      promiseResult.then((row) => {
        const result : number = row[0] as number;
        resolve(result);
      }).catch((error) => {
        // Error code for already exists
        if (error.code == '23505') {
          reject(new DatabaseError(error.message));
        } else {
          reject(error);
        }
      });
    });
  }

  updatePassword(userId: number, password: string) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.table(this._userTableName)
          .where(UserColumns.user_id_column, userId).update({user_password: password});
      promiseRow.then(() => {
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  }

  get databaseConnection() : DatabaseConnectionInterface {
    return this._databaseConnection;
  }

  set databaseConnection(dataBaseConnection: DatabaseConnectionInterface) {
    this._databaseConnection = dataBaseConnection;
  }
}
