import {Authorization} from '@model/database/authorization';

export interface AuthorizationRepositoryInterface {
    getAuthorizations(): Promise<Authorization[]>
    getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]>
}
