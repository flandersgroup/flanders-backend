import {Authorization, AuthorizationColumns} from '@model/database/authorization';
import {ProfileAuthorizationRelationColumns} from '@model/database/profileAuthorizationRelation';
import {DatabaseConnectionInterface} from '@repository/base';
import {AuthorizationRepositoryInterface} from '.';

export class AuthorizationRepository implements AuthorizationRepositoryInterface {
  private _authorizationTableName : string = 'flanders_authorization';
  private _profileAuthorizationRelationTableName : string = 'flanders_profile_authorization_relation';

  constructor(private _databaseConnection : DatabaseConnectionInterface) {
  }

  getAuthorizations(): Promise<Authorization[]> {
    return new Promise<Authorization[]>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._authorizationTableName).select();
      promiseRow.then((row) => {
        resolve(row);
      }).catch((error : any) => {
        reject(error);
      });
    });
  }

  getAuthorizationsOfProfile(profileId: number): Promise<Authorization[]> {
    return new Promise<Authorization[]>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._profileAuthorizationRelationTableName)
          .where(ProfileAuthorizationRelationColumns.profile_authorization_relation_profile_id_column, profileId)
          .rightJoin(this._authorizationTableName,
              ProfileAuthorizationRelationColumns.profile_authorization_relation_authorizaton_id_column,
              AuthorizationColumns.authorization_id_column)
          .select(AuthorizationColumns.authorization_id_column,
              AuthorizationColumns.authorization_name_column,
              AuthorizationColumns.authorization_description_column);
      promiseRow.then((row) => {
        resolve(row);
      }).catch((error : any) => {
        reject(error);
      });
    });
  }
}
