import {Knex} from 'knex';

export interface DatabaseConnectionInterface {
    connection: Knex
    startTransaction() : Promise<void>;
    commitTransaction() : void;
    rollbackTransaction() : void;
}
