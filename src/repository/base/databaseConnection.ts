import {Database} from '@lib/database';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {Knex} from 'knex';
import {DatabaseConnectionInterface} from '.';

export class DatabaseConnection implements DatabaseConnectionInterface {
  private _connection : Knex | undefined = undefined;
  private _database : Database;

  constructor(configuration: DatabaseConfiguration) {
    this._database = new Database(configuration);
  }

  get connection(): Knex {
    if (this._connection) {
      return this._connection;
    } else {
      return this._database.getConnection();
    }
  }

  startTransaction() : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this._database.getConnection().transaction().then((connection) => {
        this._connection = connection;
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  }

  commitTransaction() : void {
    if (this._connection) {
      const convertedConnection : Knex.Transaction = this._connection as Knex.Transaction;
      convertedConnection.commit();
      this._connection.destroy();
      this._connection = undefined;
    }
  }

  rollbackTransaction() : void {
    if (this._connection) {
      const convertedConnection : Knex.Transaction = this._connection as Knex.Transaction;
      convertedConnection.rollback();
      this._connection.destroy();
      this._connection = undefined;
    }
  }
}
