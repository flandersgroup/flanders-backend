import {RepositoriesInterface} from '@repository';
import {Logger} from '@lib/logger';
import {UserRepositoryInterface} from '@repository/user';
import {UserRepository} from '@repository/user/user';
import {ProfileRepositoryInterface} from './profile';
import {ProfileRepository} from './profile/profile';
import {AuthorizationRepositoryInterface} from './authorization';
import {AuthorizationRepository} from './authorization/authorization';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {DatabaseConnectionInterface} from './base';
import {DatabaseConnection} from './base/databaseConnection';

export class Repositories implements RepositoriesInterface {
  private _databaseConnection : DatabaseConnectionInterface;

  constructor(private _logger: Logger, private _databaseConfiguration : DatabaseConfiguration) {
    this._logger.info('Initialisation of the repositories');
    this._databaseConnection = new DatabaseConnection(this._databaseConfiguration);
  }

  get user() : UserRepositoryInterface {
    return new UserRepository(this._databaseConnection);
  }

  get profile() : ProfileRepositoryInterface {
    return new ProfileRepository(this._databaseConnection);
  }

  get authorization() : AuthorizationRepositoryInterface {
    return new AuthorizationRepository(this._databaseConnection);
  }

  get databaseConnection() : DatabaseConnectionInterface {
    return this._databaseConnection;
  }
}
