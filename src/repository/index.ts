import {UserRepositoryInterface} from '@repository/user';
import {ProfileRepositoryInterface} from '@repository/profile';
import {AuthorizationRepositoryInterface} from './authorization';
import {DatabaseConnectionInterface} from './base';

export interface RepositoriesInterface {
    user : UserRepositoryInterface
    profile : ProfileRepositoryInterface
    authorization : AuthorizationRepositoryInterface
    databaseConnection : DatabaseConnectionInterface
}
