import {Profile} from '@model/database/profile';
import {UserProfileRelation} from '@model/database/userProfileRelation';

export interface ProfileRepositoryInterface {
    getRegistrableProfiles() : Promise<Profile[]>;
    isProfileRegistrable(profileId : number) : Promise<boolean>;
    getProfileByName(profileName : string) : Promise<Profile | undefined>;
    getProfilesOfUser(userId : number) : Promise<Profile[]>;
    insertUserProfileRelation(userProfileRelation : UserProfileRelation) : Promise<void>;
}
