import {Profile, ProfileColumns} from '@model/database/profile';
import {UserProfileRelation, UserProfileRelationColumns} from '@model/database/userProfileRelation';
import {DatabaseConnectionInterface} from '@repository/base';
import {DatabaseError} from '@repository/error';
import {ProfileRepositoryInterface} from '.';

export class ProfileRepository implements ProfileRepositoryInterface {
  private _profileTableName : string = 'flanders_profile';
  private _userProfileRelationTableName : string = 'flanders_user_profile_relation';

  constructor(private _databaseConnection : DatabaseConnectionInterface) {
  }

  isProfileRegistrable(profileId: number): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._profileTableName)
          .select(ProfileColumns.profile_can_register_column).where(ProfileColumns.profile_id_column, profileId).first();
      promiseRow.then((row) => {
        if (row) {
          resolve(row.profile_can_register);
        } else {
          resolve(row);
        }
      }).catch((error) => {
        reject(error);
      });
    });
  }

  getRegistrableProfiles(): Promise<Profile[]> {
    return new Promise<Profile[]>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._profileTableName).where(ProfileColumns.profile_can_register_column, true);
      promiseRow.then((row) => {
        resolve(row);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  getProfileByName(profileName : string) : Promise<Profile | undefined> {
    return new Promise<Profile>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      connection.queryBuilder().table(this._profileTableName).where(ProfileColumns.profile_name_column, profileName).first().then((row) => {
        resolve(row);
      }).catch((error) => reject(error));
    });
  }

  getProfilesOfUser(userId : number) : Promise<Profile[]> {
    return new Promise<Profile[]>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      const promiseRow = connection.queryBuilder().table(this._userProfileRelationTableName)
          .where(UserProfileRelationColumns.user_profile_relation_user_id_column, userId)
          .rightJoin(this._profileTableName, UserProfileRelationColumns.user_profile_relation_profile_id_column,
              ProfileColumns.profile_id_column)
          .select(ProfileColumns.profile_id_column, ProfileColumns.profile_name_column,
              ProfileColumns.profile_description_column, ProfileColumns.profile_can_register_column);
      promiseRow.then((row) => {
        resolve(row);
      }).catch((error : any) => {
        reject(error);
      });
    });
  }

  insertUserProfileRelation(userProfileRelation : UserProfileRelation) : Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const connection = this._databaseConnection.connection;
      return connection.insert(userProfileRelation).into(this._userProfileRelationTableName).then(() => {
        resolve();
      }).catch((error) => {
        if (error.code == '23505') {
          reject(new DatabaseError(error.message));
        } else {
          reject(error);
        }
      });
    });
  }

  get databaseConnection() : DatabaseConnectionInterface {
    return this._databaseConnection;
  }

  set databaseConnection(dataBaseConnection: DatabaseConnectionInterface) {
    this._databaseConnection = dataBaseConnection;
  }
}
