import {Configuration} from '@configuration/config.global';
import {LoginMailerConfiguration} from '@controller/login/mailer/configuration';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {GoogleAuthenticationConfiguration} from '@lib/google/configuration';
import {TokenConfiguration} from '@lib/token/configuration';

export class ConfigurationTest implements Configuration {
  private _environnement: string = 'developpement';
  private _port = 3000;
  private _databaseConfiguration: DatabaseConfiguration;
  mailerConfiguration = new GoogleAuthenticationConfiguration;
  private _loginMailerConfiguration: LoginMailerConfiguration;
  private _tokenConfiguration: TokenConfiguration;

  constructor() {
    this._databaseConfiguration = {
      host: process.env.DB_HOST ?? '',
      port: 5432,
      user: 'flanders_db_test_user',
      password: 'Lw!iEjeaxW3alVFgmvm,_zBGkkqhm6',
      database: 'flanders_db_test',
    };

    this._loginMailerConfiguration = {
      frontendRegistratedApi: '/frontend/registered?tokenData=',
      frontendResetPasswordApi: '/frontend/resetPassword?tokenData=',
      encodingKey: 'bs9u9qSYyT31zyGbp5CcMhMkSaVJs5TE',
      // 24h
      registrationLinkExpiresIn: 24*60*60*60,
    };

    this._tokenConfiguration = {
      key: 'hKS2WXtJy5lT4S59vxIIsFMBhb9qrngL',
      expiresIn: 60*60,
    };
  }

  get environnement() : string {
    return this._environnement;
  }

  get port() : number {
    return this._port;
  }

  get databaseConfiguration() : DatabaseConfiguration {
    return this._databaseConfiguration;
  }

  get loginMailerConfiguration(): LoginMailerConfiguration {
    return this._loginMailerConfiguration;
  }

  get tokenConfiguration(): TokenConfiguration {
    return this._tokenConfiguration;
  }
}
