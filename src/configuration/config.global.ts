import {LoginMailerConfiguration} from '@controller/login/mailer/configuration';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {GoogleAuthenticationConfiguration} from '@lib/google/configuration';
import {TokenConfiguration} from '@lib/token/configuration';

export interface Configuration {
    environnement: string
    port: number

    loggingFilePath?: string
    logLevel?: string

    databaseConfiguration: DatabaseConfiguration
    mailerConfiguration: GoogleAuthenticationConfiguration

    loginMailerConfiguration: LoginMailerConfiguration
    tokenConfiguration: TokenConfiguration
}
