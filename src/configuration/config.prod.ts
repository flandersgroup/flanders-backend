import {Configuration} from '@configuration/config.global';
import {LoginMailerConfiguration} from '@controller/login/mailer/configuration';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {GoogleAuthenticationConfiguration} from '@lib/google/configuration';
import {TokenConfiguration} from '@lib/token/configuration';

export class ConfigurationProduction implements Configuration {
  private _environnement: string = 'production';
  private _port : number = parseInt(process.env.PORT ?? '80');
  private _databaseConfiguration: DatabaseConfiguration;
  private _logLevel = process.env.LOG_LEVEL ?? 'info';

  private _mailerConfiguration : GoogleAuthenticationConfiguration;
  private _loginMailerConfiguration: LoginMailerConfiguration;
  private _tokenConfiguration: TokenConfiguration;

  static checkUndefinedVariable(value : any, message : string) : string {
    if (value) {
      return value;
    } else {
      throw new Error(message);
    }
  }

  constructor() {
    this._databaseConfiguration = {
      connectionString: process.env.DATABASE_URL,
    };

    this._mailerConfiguration = {
      clientId: process.env.GOOGLE_CLIENTID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      accessToken: process.env.GOOGLE_ACCESS_TOKEN,
      refreshToken: process.env.GOOGLE_REFRESH_TOKEN,
      scope: process.env.MAILER_SCOPE,
    };

    this._loginMailerConfiguration = {
      frontendRegistratedApi: 'https://flanders.herokuapp.com/registered?tokenData=',
      frontendResetPasswordApi: 'https://flanders.herokuapp.com/resetPassword?tokenData=',
      encodingKey: ConfigurationProduction.checkUndefinedVariable(process.env.LOGIN_MAILER_KEY, 'LOGIN_MAILER_KEY is required'),
      // 24h
      registrationLinkExpiresIn: 24*60*60*60,
    };

    this._tokenConfiguration = {
      key: ConfigurationProduction.checkUndefinedVariable(process.env.TOKEN_KEY, 'TOKEN_KEY is required'),
      expiresIn: 60*60,
    };
  }

  get environnement() : string {
    return this._environnement;
  }

  get port(): number {
    return this._port;
  }

  get databaseConfiguration(): DatabaseConfiguration {
    return this._databaseConfiguration;
  }

  get logLevel() : string {
    return this._logLevel;
  }

  get mailerConfiguration() : GoogleAuthenticationConfiguration {
    return this._mailerConfiguration;
  }

  get loginMailerConfiguration(): LoginMailerConfiguration {
    return this._loginMailerConfiguration;
  }

  get tokenConfiguration(): TokenConfiguration {
    return this._tokenConfiguration;
  }
}
