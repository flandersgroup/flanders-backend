import {Configuration} from '@configuration/config.global';
import {LoginMailerConfiguration} from '@controller/login/mailer/configuration';
import {DatabaseConfiguration} from '@lib/database/configuration';
import {GoogleAuthenticationConfiguration} from '@lib/google/configuration';
import {TokenConfiguration} from '@lib/token/configuration';

export class ConfigurationDevelopement implements Configuration {
  private _environnement: string = 'developpement';
  private _port = parseInt(process.env.APP_PORT ?? '80');
  private _databaseConfiguration: DatabaseConfiguration;

  private _mailerConfiguration: GoogleAuthenticationConfiguration;

  private _loginMailerConfiguration: LoginMailerConfiguration;
  private _tokenConfiguration: TokenConfiguration;

  constructor() {
    this._databaseConfiguration = {
      host: process.env.DB_HOST ?? '',
      port: 5432,
      user: 'flanders_db_int_user',
      password: 'z?%O@iN2KnAN?G io7bUWX19V,4UEw',
      database: 'flanders_db_int',
    };

    this._mailerConfiguration = {
      clientId: process.env.GOOGLE_CLIENTID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      refreshToken: process.env.GOOGLE_REFRESH_TOKEN,
      accessToken: process.env.GOOGLE_ACCESS_TOKEN,
      scope: process.env.MAILER_SCOPE,
    };

    this._loginMailerConfiguration = {
      frontendRegistratedApi: 'http://localhost:4200/registered?tokenData=',
      frontendResetPasswordApi: 'http://localhost:4200/resetPassword?tokenData=',
      encodingKey: 'bs9u9qSYyT31zyGbp5CcMhMkSaVJs5TE',
      // 24h
      registrationLinkExpiresIn: 24*60*60*60,
    };

    this._tokenConfiguration = {
      key: 'hKS2WXtJy5lT4S59vxIIsFMBhb9qrngL',
      expiresIn: 60*60,
    };
  }

  get environnement(): string {
    return this._environnement;
  }

  get port(): number {
    return this._port;
  }

  get databaseConfiguration(): DatabaseConfiguration {
    return this._databaseConfiguration;
  }

  get mailerConfiguration(): GoogleAuthenticationConfiguration {
    return this._mailerConfiguration;
  }

  get loginMailerConfiguration(): LoginMailerConfiguration {
    return this._loginMailerConfiguration;
  }

  get tokenConfiguration(): TokenConfiguration {
    return this._tokenConfiguration;
  }
}
