import {Configuration} from '@configuration/config.global';
import {ConfigurationProduction} from '@configuration/config.prod';
import {ConfigurationDevelopement} from '@configuration/config.development';
import {ConfigurationTest} from '@configuration/config.test';

export class ConfigurationLoader {
  static configuration(environnement: string | undefined): Configuration {
    if (environnement === undefined || environnement === 'developement') {
      return new ConfigurationDevelopement();
    } else if (environnement === 'production') {
      return new ConfigurationProduction();
    } else if (environnement === 'test') {
      return new ConfigurationTest();
    } else {
      throw new Error('Unknown environnement type : ' + environnement + ', unable to load corresponding configuration');
    }
  }
}
