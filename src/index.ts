
import {Application} from '@application';

const application = new Application();
application.setup(process.env.NODE_ENV);
export default application.application;
