export class ProfileAuthorizationRelation {
  profile_authorization_relation_profile_id:number=NaN;
  profile_authorization_relation_authorizaton_id:number=NaN;
}

export class ProfileAuthorizationRelationColumns {
  static profile_authorization_relation_profile_id_column:string='profile_authorization_relation_profile_id';
  static profile_authorization_relation_authorizaton_id_column:string='profile_authorization_relation_authorizaton_id';
}
