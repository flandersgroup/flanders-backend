export class Profile {
  profile_id : number|undefined= NaN;
  profile_name : string = '';
  profile_description : string = '';
  profile_can_register: boolean = false;
}

export class ProfileColumns {
  static profile_id_column : string = 'profile_id';
  static profile_name_column : string = 'profile_name';
  static profile_description_column : string = 'profile_description';
  static profile_can_register_column: string = 'profile_can_register';
}
