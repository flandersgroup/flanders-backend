export class User {
  user_id: number | undefined = undefined;
  user_email : string = '';
  user_password : string = '';
  user_first_name : string = '';
  user_last_name : string = '';
  user_enabled : boolean = true;
}

export class UserColumns {
  static user_id_column : string = 'user_id';
  static user_email_column : string = 'user_email';
  static user_password_column : string = 'user_password';
  static user_first_name_column : string = 'user_first_name';
  static user_last_name_column : string = 'user_last_name';
  static user_enabled_column : string = 'user_enabled';
}
