
export class UserProfileRelation {
  user_profile_relation_user_id : number = NaN;
  user_profile_relation_profile_id : number = NaN;
}

export class UserProfileRelationColumns {
  static user_profile_relation_user_id_column : string = 'user_profile_relation_user_id';
  static user_profile_relation_profile_id_column : string = 'user_profile_relation_profile_id';
}
