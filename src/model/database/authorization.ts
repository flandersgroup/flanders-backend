export class Authorization {
  authorization_id:number= NaN;
  authorization_name:string= '';
  authorization_description:string='';
}

export class AuthorizationColumns {
  static authorization_id_column : string = 'authorization_id';
  static authorization_name_column: string = 'authorization_name';
  static authorization_description_column: string = 'authorization_description';
}
