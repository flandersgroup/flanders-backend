#!/bin/bash

echo "Start of initialization"
base_dir='/usr/src/app/sql/'
cd /usr/src/app/sql/
/bin/bash InitializeDatabase.sh --postgresql psql --user $POSTGRES_USER --password password --base_dir $base_dir
echo "Initialization finished"