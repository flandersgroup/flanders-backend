#!/bin/sh

isReady=$(docker logs flanders-backend | grep -o 'Server connected')
timeout=10

while [[ -z "$isReady" ]] && [ $timeout != 0 ]; do
    sleep 1
    isReady=$(docker logs flanders-backend | grep -o 'Server connected')
    timeout=$((timeout-1))
    echo "Wait try $timeout/10"
done