#!/bin/sh

finishPhrase="Initialization finished"
isFinished=$( docker-compose logs --tail 50 postgres-database | grep "$finishPhrase" )
TIMEOUT=10

while [[ -z "$isFinished" ]] && [ $TIMEOUT != 0 ]; do
    sleep 1
    isFinished=$( docker-compose logs --tail 50 postgres-database | grep "$finishPhrase" )
    TIMEOUT=$((TIMEOUT-1))
    echo "Wait try $TIMEOUT/10"
done