#!/bin/sh

numberOfRequest=100
numberOfParrallelRequest=10
times=()

currentNumberOfRequest=$numberOfRequest
tempFiles=()
while [ "$currentNumberOfRequest" != 0 ]
do
    currentParrallelRequest="$numberOfParrallelRequest"
    while [ "$currentNumberOfRequest" != 0 ] && [ "$currentParrallelRequest" != 0 ]
    do
        tempFile="output$currentNumberOfRequest"
        curl --location --request POST 'http://localhost:8080/login' --header 'Content-Type: application/json' --data-raw '{"email": "vincentbaijot@gmail.com","password":"$2a$10$XbMt.meoZlp4zLN2PgvJDO3R5.XFJYUDkP2keGF97BIS3ypjBkdKG"}' -s -o /dev/null -w '%{time_total}\n' >> $tempFile &
        tempFiles[$currentNumberOfRequest]="$tempFile"
        currentParrallelRequest=$((currentParrallelRequest-1))
        currentNumberOfRequest=$((currentNumberOfRequest-1))
    done
    wait
done

i=0
for tempFile in ${tempFiles[@]}
do
    times[i]=$(cat $tempFile)
    rm $tempFile
    i=$((i+1))
done

printf '%s;' "${times[@]}"

exit 0