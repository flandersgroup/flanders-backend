CREATE USER flanders_db_test_user WITH PASSWORD 'Lw!iEjeaxW3alVFgmvm,_zBGkkqhm6';
GRANT CONNECT ON DATABASE flanders_db_test TO flanders_db_test_user;
GRANT SELECT, INSERT, DELETE, UPDATE ON ALL TABLES IN SCHEMA public TO flanders_db_test_user;
GRANT USAGE, SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA public TO flanders_db_test_user;