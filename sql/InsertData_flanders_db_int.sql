/********************************************
* Insert data in the user tables
********************************************/

INSERT INTO flanders_profile (profile_id, profile_name, profile_description, profile_can_register) VALUES 
(1,'Administrator', 'Administrator of the website, able to administrate users', false),
(2,'Vilain', 'A vilain who can edit his profile and receive missions', false),
(3,'Hero', 'A hero who can submit mission for vilains', true),
(4,'Candidate', 'A candidate to become a vilain', true);

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_profile','profile_id'),4);

INSERT INTO flanders_authorization (authorization_id, authorization_name, authorization_description) VALUES 
(1,'CanAdministrateUsers', 'Can create, update, remove user and give them access authorization'),
(2,'CanSeeHisPlanning', 'Can consult his planning'),
(3,'CanSeeHisAssignedMissions', 'Can consult the missions that are assigned to him'),
(4,'CanSeeHisSubmittedMissions', 'Can consult the missions he submitted'),
(5,'CanSubmitMission', 'Can submit mission'),
(6,'CanSeeHisProfile', 'Can see his candidate profile');

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_authorization','authorization_id'),6);

INSERT INTO flanders_profile_authorization_relation (profile_authorization_relation_profile_id, profile_authorization_relation_authorizaton_id) VALUES 
(1,1), (2,2), (2,3), (3,4), (3,5), (4,6);

INSERT INTO flanders_user (user_id, user_email, user_password, user_first_name, user_last_name) VALUES 
(1,'vincentbaijot@gmail.com', '$2a$10$vsA6FmoW9didwIUD/f/2MOgPb9GiPGYrfdt7vhuaVcZ4sNpkPKSly', 'vincent', 'baijot'),
(2,'vilain@gmail.com', '$2a$10$dc80WrUmbqN8a3bHAllMDOsDJT57U.aVqLgC826q2edJTvMrksa/q', 'vilain', 'profile'),
(3,'hero@gmail.com', '$2a$10$Bryr1BUi8ddBoySKuVzECO6HnD09QK7L2okYJ9KdZP6lsfNKZWYDy', 'hero', 'profile'),
(4,'candidate@gmail.com', '$2a$10$LMsRwcE7KSWq2CpyRw.piuRT8DEA/WMlGIdL44HtxVj5HMCD5WPd6', 'candidate', 'profile');

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_user','user_id'),4);

INSERT INTO flanders_user_profile_relation (user_profile_relation_user_id, user_profile_relation_profile_id) VALUES 
(1,1), (1,2), (2,2), (3,3), (4,4);