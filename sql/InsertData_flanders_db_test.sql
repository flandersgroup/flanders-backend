/********************************************
* Insert data in the user tables
********************************************/

INSERT INTO flanders_profile (profile_id, profile_name, profile_description, profile_can_register) VALUES 
(1,'Administrator', 'Administrator of the website, able to administrate users', false),
(2,'Vilain', 'A vilain who can edit his profile and receive missions', false),
(3,'Visitor', 'A visitor', true),
(4,'Hero', 'A hero who can submit mission for vilains', true),
(5,'Candidate', 'A candidate to become a vilain', true);

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_profile','profile_id'),5);

INSERT INTO flanders_authorization (authorization_id, authorization_name, authorization_description) VALUES 
(1,'CanAdministrateUsers', 'Can create, update, remove user and give them access authorization'),
(2,'CanSeeHisPlanning', 'Can consult his planning'),
(3,'CanSeeHisAssignedMissions', 'Can consult the missions that are assigned to him'),
(4,'CanSeeHisSubmittedMissions', 'Can consult the missions he submitted'),
(5,'CanSubmitMission', 'Can submit mission'),
(6,'CanSeeHisProfile', 'Can see his candidate profile');

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_authorization','authorization_id'),6);

INSERT INTO flanders_profile_authorization_relation (profile_authorization_relation_profile_id, profile_authorization_relation_authorizaton_id) VALUES 
(1,1), (2,2), (2,3), (4,4), (4,5), (5,6);

INSERT INTO flanders_user (user_id, user_email, user_password, user_first_name, user_last_name) VALUES 
(1,'admin.profile1@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'admin', 'user'),
(2,'admin.profile2@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'admin', 'user'),
(3,'admin.profile3@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'admin', 'user'),
(4,'visitor.profile1@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'visitor', 'user'),
(5,'visitor.profile2@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'visitor', 'user'),
(6,'visitor.profile3@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'visitor', 'user'),
(7,'hero.profile3@test.com', '$2a$10$Gp5UV.kqL4vKYpqN.iAsUeU/puBhTvbWy0NnwGixM5FQszKMc0T/O', 'hero', 'user');

/* Multiple insert cause desync of index, so force the index value */
SELECT setval(pg_get_serial_sequence('flanders_user','user_id'),7);

INSERT INTO flanders_user_profile_relation (user_profile_relation_user_id, user_profile_relation_profile_id) VALUES 
(1,1), (1,2), (2,1), (3,1), (4,3), (5,3), (6,3), (7,4);