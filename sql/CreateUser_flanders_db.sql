CREATE USER flanders_db_application_user WITH PASSWORD '&2rfEQbcsoSZtFtGv KJR8MoK,wrFi';
GRANT CONNECT ON DATABASE flanders_db TO flanders_db_application_user;
GRANT SELECT, INSERT, DELETE, UPDATE ON ALL TABLES IN SCHEMA public TO flanders_db_application_user;
GRANT USAGE, SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA public TO flanders_db_application_user;