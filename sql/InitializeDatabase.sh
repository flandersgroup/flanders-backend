#!/bin/bash

echo "Start to initialize database"

# Exit on any command fail !
set -e
 
function usage(){
	printf "Initialize the databases of Flanders company\n"
	printf "You can give the parameters in command line option or you need to fill it at the script execution\n"
	printf "\t--postgresql       : Path to postgresql.exe, for default install of postgresql it is /c/Program\ Files/postgresql/postgresql\ Workbench\ 8.0/postgresql.exe \n"
	printf "\t--host        : Host on which postgresql server is located \n"
	printf "\t--user        : postgresql user name to use when connecting to server \n"
	printf "\t--password    : Password to use when connecting to server\n"
	printf "\t--base_dir    : The directory containing the sql files\n"
	printf "\t-h            : Display the help \n"
}
 
OPTS=$( getopt -o h -l postgresql:,host:,user:,password:,base_dir: -- "$@" )

if [ $? != 0 ]
then
    exit 1
fi
 
eval set -- "$OPTS"

while true ; do
    case "$1" in
        -h) 		usage;
					exit 0;;
		--postgresql) 	postgresql="$2"
					shift 2;;
        --host) 	host="$2";
					shift 2;;
		--user) 	user="$2";
					shift 2;;
		--password) password="$2";
					shift 2;;
		--base_dir) base_dir="$2";
					shift 2;;
		-- )  shift; break ;;
    esac
done

if [[ -z $base_dir ]]
then
	base_dir=$(dirname "$0")
fi

check_parameters () {

	if [[ -z $postgresql ]]
	then
		echo "Path to postgresqlsh.exe"
		read -p 'postgresql : ' postgresql
	fi
	
	if [[ -z $user ]]
	then
		echo "Please enter the user of the postgresql server"
		read -p 'User : ' user
	fi
	
	if [[ -z $password ]]
	then
		echo "Please enter the password of the postgresql server"
		read -s -p 'Password : ' password
	fi
}


execute_sql_query () {
	if [[ -z $host ]]
	then
		echo "PGPASSWORD=$password psql -e --username=$user $1 -c \"$2\""
		PGPASSWORD=$password psql --username=$user $1 -c "$2"
	else
		echo "PGPASSWORD=$password psql -e --username=$user --host=$host $1 -c \"$2\""
		PGPASSWORD=$password psql --username=$user --host=$host $1 -c "$2"
	fi
}

execute_sql_file () {
	if [[ -z $host ]]
	then
		PGPASSWORD=$password psql --username=$user $1 -f $2
	else
		PGPASSWORD=$password psql --username=$user --host=$host $1 -f $2
	fi
}

check_parameters

for database in "flanders_db" "flanders_db_int" "flanders_db_test"
do
echo ""
echo "Create the database : $database"
execute_sql_query "" "CREATE DATABASE ${database}"
echo "Create the tables"
execute_sql_file "--dbname=${database}" "$base_dir/CreateTables.sql"
echo "Insert data to tables from file InsertData_${database}.sql"
execute_sql_file "--dbname=${database}" "$base_dir/InsertData_${database}.sql"
echo "Create user from file CreateUser_${database}.sql"
execute_sql_file "--dbname=${database}" "$base_dir/CreateUser_${database}.sql"
done
 
exit 0