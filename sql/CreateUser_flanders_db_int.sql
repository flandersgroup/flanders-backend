CREATE USER flanders_db_int_user WITH PASSWORD 'z?%O@iN2KnAN?G io7bUWX19V,4UEw';
GRANT CONNECT ON DATABASE flanders_db_int TO flanders_db_int_user;
GRANT SELECT, INSERT, DELETE, UPDATE ON ALL TABLES IN SCHEMA public TO flanders_db_int_user;
GRANT USAGE, SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA public TO flanders_db_int_user;