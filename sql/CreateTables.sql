/********************************************
* Create the tables linked to the user
********************************************/

CREATE TABLE flanders_profile (
  profile_id SERIAL NOT NULL,
  profile_name VARCHAR(255) UNIQUE NOT NULL,
  profile_description TEXT NOT NULL,
  profile_can_register BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (profile_id)
);

CREATE TABLE flanders_authorization (
  authorization_id SERIAL NOT NULL,
  authorization_name VARCHAR(255) UNIQUE NOT NULL,
  authorization_description TEXT,
  PRIMARY KEY (authorization_id)
);

CREATE TABLE flanders_profile_authorization_relation (
  profile_authorization_relation_profile_id INT NOT NULL,
  profile_authorization_relation_authorizaton_id INT NOT NULL,
  UNIQUE (profile_authorization_relation_profile_id, profile_authorization_relation_authorizaton_id),
  FOREIGN KEY (profile_authorization_relation_profile_id) REFERENCES flanders_profile(profile_id),
  FOREIGN KEY (profile_authorization_relation_authorizaton_id) REFERENCES flanders_authorization(authorization_id)
);

CREATE TABLE flanders_user (
  user_id SERIAL NOT NULL,
  user_email VARCHAR(255) UNIQUE NOT NULL,
  user_password TEXT NOT NULL,
  user_first_name VARCHAR(255),
  user_last_name VARCHAR(255),
  user_enabled BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (user_id)
);

CREATE TABLE flanders_user_profile_relation (
  user_profile_relation_user_id INT NOT NULL,
  user_profile_relation_profile_id INT NOT NULL,
  UNIQUE (user_profile_relation_user_id, user_profile_relation_profile_id),
  FOREIGN KEY (user_profile_relation_user_id) REFERENCES flanders_user(user_id),
  FOREIGN KEY (user_profile_relation_profile_id) REFERENCES flanders_profile(profile_id)
);